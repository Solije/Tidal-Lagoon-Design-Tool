#include "meshview.h"
#include <iostream>

#include <QMouseEvent>
#include <QRubberBand>
#include <QFileDialog>
#include <QTimer>
#include <QGLWidget>
#include <QMenu>


#include "Parser/geoparser.h"
#include "Parser/mshparser.h"
#include "Mesh/mesh.h"
#include "Mesh/meshedge.h"
#include "UI/mainwindow.h"

///////////////////////////////////////////
///////////////////////////////////////////
// Mesh Construction
///////////////////////////////////////////
///////////////////////////////////////////

MeshView::MeshView(QWidget *parent) : QGraphicsView(parent)
{
  //this->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));

  //When a new vertex is selected, show it's coords in the ui
  connect(this, SIGNAL(updateActiveVertex(int)), this->window(), SIGNAL(refreshVertexCoords(int)) );
  setDragMode(QGraphicsView::ScrollHandDrag);
  setOptimizationFlag(QGraphicsView::DontSavePainterState);

}

MeshView::~MeshView(){}

///////////////////////////////////////////
// Mesh loading functions
///////////////////////////////////////////

bool MeshView::loadMesh(){
  QString filePath = QFileDialog::getOpenFileName(this, tr("Open Mesh File"), "~/" , tr("Geo Files (*.geo);;Mesh Files (*.msh)"));
  return loadMesh(filePath);
}

bool MeshView::loadMesh(std::string filePath){
  return loadMesh(QString(filePath.c_str()));
}

bool MeshView::loadMesh(QString filePath){
  if (!filePath.isNull()){

    //Load Mesh object
    clearMesh();
    QString fileExt = filePath.split(".").back();
    std::cout << fileExt.toStdString() << std::endl;
    if (fileExt == "geo"){
        GeoParser parser;
        activeMesh = parser.parse(filePath.toStdString(), this);
    } else if (fileExt == "msh"){
        MshParser parser;
        activeMesh = parser.parse(filePath.toStdString(), this);
    }

    std::cout << "New file loaded" << std::endl;
    this->setFrameRect(this->scene()->sceneRect().toRect());
    this->refreshView();
    emit newMeshLoaded(activeMesh);
    return true;
  }
  return false;
}

void MeshView::loadEmptyMesh(){
  clearMesh();
  activeMesh = new Mesh(this);

  std::cout << "Empty mesh loaded" << std::endl;
  setFrameRect(scene()->sceneRect().toRect());
  //refreshView();
  emit newMeshLoaded(activeMesh);
}

bool MeshView::insertMesh(){
  QString filePath = QFileDialog::getOpenFileName(this, tr("Open Mesh File"), "~/" , tr("Geo Files (*.geo);;Mesh Files (*.msh)"));
  if (!filePath.isNull()){

    //Load Mesh object
    QString fileExt = filePath.split(".").back();
    std::cout << fileExt.toStdString() << std::endl;
    if (fileExt == "geo"){
        GeoParser parser;
        activeMesh = parser.insertMesh(filePath.toStdString(), getActiveMesh());
    } else if (fileExt == "msh"){
        //MshParser parser;
        //activeMesh.reset(parser.insertMesh(filePath.toStdString(), getActiveMesh()));
    }

    std::cout << "New file loaded" << std::endl;
    this->setFrameRect(this->scene()->sceneRect().toRect());
    this->refreshView();
    emit newMeshLoaded(activeMesh);
    return true;
  }
  return false;
}

bool MeshView::clearMesh(){
  if (activeMesh){
    for (Vertex* vert : activeMesh->getVertices()){
      vert->hide();
      scene()->removeItem(vert);
    }
    for(std::shared_ptr<MeshEdge> line: activeMesh->getLines()){
      line->hide();
      scene()->removeItem(line.get());
    }
    emit newMeshLoaded(nullptr);
    delete activeMesh;
    activeMesh = nullptr;
    return true;
  }
  return false;
}

void MeshView::setActiveMesh(Mesh* newMesh){
  activeMesh = newMesh;
}

///////////////////////////////////////////
// Adding MeshElements methods
///////////////////////////////////////////

void MeshView::addVertexToScene(Vertex* newVertex){
  this->scene()->addItem(newVertex);

  //When a vertex is selected, mark it as active
  connect(newVertex, SIGNAL(vertexSelected(int)), this, SLOT(setActiveVertex(int)));

  //Refresh coordinates shown in ui when moving vertex
  connect(newVertex, SIGNAL(vertexBeingMoved(int)), this->window(), SIGNAL(refreshVertexCoords(int)));

  //When a vertex has been moved and dropped. Add a change to the changelog
  connect(newVertex, SIGNAL(vertexDropped(int)), this->window(), SIGNAL(addVertexMovement(int)));

}

void MeshView::addLineToScene(std::shared_ptr<MeshEdge> newLine){
  this->scene()->addItem(newLine.get());
}



///////////////////////////////////////////
// "Get" functions
///////////////////////////////////////////

Mesh* MeshView::getActiveMesh(){
  return activeMesh;
}

Vertex* MeshView::getActiveVertex(){
  return activeVertex;
}

///////////////////////////////////////////
// Mesh drawing functions
///////////////////////////////////////////

void MeshView::refreshView(){
  //Sets MeshEdges visible
  activeMesh->setHidden(false);
  for(std::shared_ptr<MeshEdge> p : activeMesh->getLines()){
    p->setVisible(true);
    p->updateLine();
  }
  this->viewport()->update();
}

void MeshView::setLineVisibility(bool condition){
  activeMesh->setHidden(!condition);
  for(std::shared_ptr<MeshEdge> p : activeMesh->getLines()){
    p->setVisible(condition);
  }
}

///////////////////////////////////////////
// Vertex Selection functions
///////////////////////////////////////////

void MeshView::setActiveVertex(int vertexNumber){
  //Check for whether a mesh is loaded into the view before setting vertex inactive
  //Will otherwise crash program
  switch (viewMode){
  case MeshView::EditMode:
    if (!activeVertex){
      activeVertex = activeMesh->getVertexFromNumber(vertexNumber);
      activeVertex->setActiveVertex(true);
      emit updateActiveVertex(vertexNumber);
    } else if (activeVertex->vertexNumber != vertexNumber){
      scene()->clearSelection();
      activeVertex->setActiveVertex(false);
      activeVertex = activeMesh->getVertexFromNumber(vertexNumber);
      activeVertex->setActiveVertex(true);
      activeVertex->setSelected(true);
      emit updateActiveVertex(vertexNumber);
    }
  case MeshView::SelectionMode:
    selectedVertices.push_back(getActiveMesh()->getVertexFromNumber(vertexNumber));
    for (Vertex* vert : selectedVertices){
      std::cout << vert->vertexNumber << std::endl;
    }
  }
}

QRubberBand* selectionBand = nullptr;
QPoint startPoint;

void MeshView::mousePressEvent(QMouseEvent * event)
{
  if (viewMode == MeshView::EditMode){

    //Rubberband Selection
    if (event->modifiers() == Qt::CTRL){
      //Draw selection rectangle if ctrl key is held down
      setDragMode(QGraphicsView::NoDrag);
      selectionBand = new QRubberBand(QRubberBand::Rectangle, this);
      startPoint = event->pos();
      selectionBand->setGeometry(QRect(startPoint,QSize()));
      selectionBand->show();
      return;

      //Open Context menu
    } else if (event->button() == Qt::RightButton){
      if (itemAt(event->pos())){
        //Clicked on an object in the view
        //Pass the event through to be handled by the object
        QGraphicsView::mousePressEvent(event);
      }
      else{
        QMenu menu;
        QAction *deleteAction = menu.addAction("Add new vertex here");
        QAction *selectedAction = menu.exec(event->screenPos().toPoint());
        if (selectedAction == deleteAction){
          getActiveMesh()->addVertex(mapToScene(mapFromGlobal(event->screenPos().toPoint())));
        }
      }
    }
    else{
      //If click on background, clear selection
      if (!itemAt(event->pos())){
        selectedVertices.clear();
      }
      setDragMode(QGraphicsView::ScrollHandDrag);
      QGraphicsView::mousePressEvent(event);
    }
  }


  else if (viewMode == MeshView::SelectionMode){
    if (event->button() == Qt::RightButton){
       QMenu menu;
       QAction *splineAction = menu.addAction("Create Spline");
       QAction *selectedAction = menu.exec(event->screenPos().toPoint());
       if (selectedAction == splineAction){
         getActiveMesh()->addSpline(selectedVertices);
         selectedVertices.clear();
       }
    }
    else if (event->modifiers() == Qt::CTRL){
      selectedVertices.clear();
    }
    else{
      //otherwise, pan as normal
      setDragMode(QGraphicsView::ScrollHandDrag);
      QGraphicsView::mousePressEvent(event);
    }
  }
}

void MeshView::mouseMoveEvent(QMouseEvent *event){
  if (selectionBand != nullptr){
    selectionBand->setGeometry(QRect(startPoint, event->pos()).normalized());
    QPainterPath selectionArea = QPainterPath();
    QRectF selectionRect = QRectF(mapToScene(startPoint), mapToScene(event->pos())).normalized();
    selectionArea.addRect(selectionRect);
    scene()->setSelectionArea(selectionArea, Qt::ContainsItemShape);
  }
  QGraphicsView::mouseMoveEvent(event);
}

void MeshView::mouseReleaseEvent(QMouseEvent * event){

  if (selectionBand != nullptr){
    selectionBand->setGeometry(QRect(startPoint, event->pos()).normalized());
    QPainterPath selectionArea = QPainterPath();
    QRectF selectionRect = QRectF(mapToScene(startPoint), mapToScene(event->pos())).normalized();
    selectionArea.addRect(selectionRect);
    scene()->setSelectionArea(selectionArea, Qt::ContainsItemShape);
    for (QGraphicsItem* item : scene()->selectedItems()){
      Vertex* vert = dynamic_cast<Vertex*>(item);
      if (vert){
        selectedVertices.push_back(vert);
      }
    }
    delete selectionBand;
    selectionBand = nullptr;
  }
  QGraphicsView::mouseReleaseEvent(event);

}

///////////////////////////////////////////
// View Modes functions
///////////////////////////////////////////

void MeshView::setViewMode(int newViewMode){
  int numberOfModes = 2;
  if(newViewMode < numberOfModes){
    selectedVertices.clear();
    viewMode = newViewMode;
  }
  else{
    std::cout << "Tried to set MeshView to an invalid mode" << std::endl;
  }
}

void MeshView::cycleViewMode(){
  int numberOfModes = 2;
  std::cout << viewMode << std::endl;
  if(viewMode < numberOfModes - 1){
    selectedVertices.clear();
    viewMode++;
  }
  else{
    //reset to zero
    setViewMode(MeshView::EditMode);
  }
}




///////////////////////////////////////////
///////////////////////////////////////////
// Zoom/Rotate/Reflect
///////////////////////////////////////////
///////////////////////////////////////////

///////////////////////////////////////////
// Zoom
///////////////////////////////////////////

void MeshView::zoom(double zoomFactor){
  scalefactor = zoomFactor;
  QTransform newMatrix = reflectionState * QTransform::fromScale(scalefactor, scalefactor) * rotationState;
  setTransform(newMatrix);
  emit viewZoomed(scalefactor);
}

void MeshView::wheelEvent(QWheelEvent *event)
{
  //Check that mesh is loaded
  if(activeMesh){
    if(event->delta() > 0)
    {
        //zoom in
        zoom(1.1 * scalefactor);
    }
    else
    {
        //zoom out
        zoom(0.9 * scalefactor);

    }
    emit viewZoomed(scalefactor);

    //Want to hide lines to prevent unnecessary render updates.
    //Don't hide lines if already hidden
    if(!activeMesh->isHidden()){
      setLineVisibility(false);
    }

    //Set a timer to reshow the lines once scrolling has stopped
    //If a timer already exists, reset it.
    delete zoomLineVisibilityTimer;
    zoomLineVisibilityTimer = new QTimer();
    zoomLineVisibilityTimer->setSingleShot(true);
    connect(zoomLineVisibilityTimer, SIGNAL(timeout()), this, SLOT(refreshView()));
    zoomLineVisibilityTimer->start(1000);
  }
}

///////////////////////////////////////////
// Rotate
///////////////////////////////////////////

void MeshView::rotateView(int angle){
  rotationState.rotate(angle);
  zoom(scalefactor);
}

void MeshView::setViewRotation(int angle){
  rotationState = QTransform().rotate(angle);
  zoom(scalefactor);
}

///////////////////////////////////////////
// Reflect
///////////////////////////////////////////

void MeshView::verticalFlip(){
  reflectionState.scale(1,-1);
  refreshView();
}

void MeshView::horizontalFlip(){
  reflectionState.scale(-1,1);
  refreshView();
}
