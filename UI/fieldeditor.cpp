#include "UI/fieldeditor.h"

#include <iostream>

#include <QtWidgets>

#include "Mesh/Fields/field.h"
#include "Mesh/Fields/attractorfield.h"
#include "Mesh/Fields/thresholdfield.h"

FieldEditor::FieldEditor(QWidget* parent) : QWidget(parent)
{
  QVBoxLayout *mainLayout = new QVBoxLayout;
  QHBoxLayout *topLayout = new QHBoxLayout;
  fieldLayout = new QFormLayout;
  QHBoxLayout *bottomLayout = new QHBoxLayout;

  mainLayout->setAlignment(Qt::AlignTop);
  mainLayout->insertLayout(0,topLayout);
  mainLayout->insertLayout(1,fieldLayout);
  mainLayout->insertLayout(2,bottomLayout);

  newFieldButton = new QPushButton("New Field", this);
  topLayout->addWidget(newFieldButton);
  fieldSelectionBox = new QComboBox(this);
  topLayout->addWidget(fieldSelectionBox);
  fieldSelectionBox->insertItem(0, "Existing Fields");

  fieldTypeBox = new QComboBox(this);
  QStringList fieldTypes = QStringList() << "Attractor" << "Threshold";
  fieldTypeBox->addItems(fieldTypes);
//  fieldTypeBox->setVisible(false);
  mainLayout->insertWidget(1,fieldTypeBox);

  saveFieldButton = new QPushButton("Save Field", this);
  deleteFieldButton = new QPushButton("Delete Field", this);
  bottomLayout->addWidget(saveFieldButton);
  bottomLayout->addWidget(deleteFieldButton);
//  saveFieldButton->setVisible(false);
//  deleteFieldButton->setVisible(false);

  setLayout(mainLayout);

  connect(fieldTypeBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setUI(QString)));
  connect(newFieldButton, SIGNAL(clicked(bool)), this, SLOT(on_New_Field_pushButton_clicked()));
  connect(saveFieldButton, SIGNAL(clicked(bool)), this, SLOT(on_Save_Field_pushButton_clicked()));
  connect(fieldSelectionBox, SIGNAL(currentIndexChanged(int)), this, SLOT(loadField(int)));


}

FieldEditor::~FieldEditor(){
}

///////////////////////////////////////////
// Setting up UI
///////////////////////////////////////////

void FieldEditor::setUI(QString string){
  std::cout << string.toStdString() << std::endl;
  if (string == "Attractor"){
    setAttractorUI();
  }
  else if (string == "Threshold") {
    setThresholdUI();
  }

}

void FieldEditor::clearUI(){
  QLayoutItem *item;
  while ((item = fieldLayout->takeAt(0))){
    delete item->widget();
    delete item;
  }
}

void FieldEditor::setThresholdUI(){
  fieldTypeBox->setCurrentText("Threshold");
  clearUI();

  fieldName_lineEdit = new QLineEdit(this);
  DistMax_spinBox = new QSpinBox(this);
  DistMin_spinBox = new QSpinBox(this);
  IField_spinBox = new QSpinBox(this);
  LcMax_spinBox = new QSpinBox(this);
  LcMin_spinBox = new QSpinBox(this);
  sigmoid_checkBox = new QCheckBox("Sigmoid",this);
  StopAtDistMax_checkBox = new QCheckBox("StopAtDistMax", this);
  bgField_checkBox = new QCheckBox("Background Field",this);

  fieldLayout->addRow("Field Name:", fieldName_lineEdit);
  fieldLayout->addRow("DistMax:", DistMax_spinBox);
  fieldLayout->addRow("DistMin:", DistMin_spinBox);
  fieldLayout->addRow("IField:", IField_spinBox);
  fieldLayout->addRow("LcMax:", LcMax_spinBox);
  fieldLayout->addRow("LcMin:", LcMin_spinBox);
  fieldLayout->addWidget(sigmoid_checkBox);
  fieldLayout->addWidget(StopAtDistMax_checkBox);
  fieldLayout->addWidget(bgField_checkBox);

}

void FieldEditor::setAttractorUI(){
  fieldTypeBox->setCurrentText("Attractor");
  clearUI();

  fieldName_lineEdit = new QLineEdit(this);
  EdgesList_lineEdit = new QLineEdit(this);
  FacesList_lineEdit = new QLineEdit(this);
  FieldX_spinBox = new QSpinBox(this);
  FieldY_spinBox = new QSpinBox(this);
  FieldZ_spinBox  = new QSpinBox(this);
  NNodesByEdge_spinBox = new QSpinBox(this);
  NodesList_lineEdit= new QLineEdit(this);
  bgField_checkBox = new QCheckBox("Background Field",this);

  fieldLayout->addRow("Field Name:", fieldName_lineEdit);
  fieldLayout->addRow("EdgesList:", EdgesList_lineEdit);
  fieldLayout->addRow("FacesList:", FacesList_lineEdit);
  fieldLayout->addRow("FieldX:", FieldX_spinBox);
  fieldLayout->addRow("FieldY:", FieldY_spinBox);
  fieldLayout->addRow("FieldZ:", FieldZ_spinBox);
  fieldLayout->addRow("NNodesByEdge:", NNodesByEdge_spinBox);
  fieldLayout->addRow("NodesList:", NodesList_lineEdit);
  fieldLayout->addWidget(bgField_checkBox);

}

///////////////////////////////////////////
// Field Loading
///////////////////////////////////////////

void FieldEditor::on_New_Field_pushButton_clicked(){
  setUI(fieldTypeBox->currentText());
  fieldTypeBox->setVisible(true);
  saveFieldButton->setVisible(true);
  deleteFieldButton->setVisible(true);
}

void FieldEditor::loadField(int index){
  std::cout << index << std::endl;
  clearUI();
  if (index == 0){
    return;
  }
  Field* loadedField = fieldList[index-1];
  if (dynamic_cast<AttractorField*>(loadedField) != nullptr){
    setAttractorUI();
    loadAttractorField(dynamic_cast<AttractorField*>(loadedField));
   }
  if (dynamic_cast<ThresholdField*>(loadedField) != nullptr){
    setThresholdUI();
    loadThresholdField(dynamic_cast<ThresholdField*>(loadedField));
  }
}

void FieldEditor::loadAttractorField(AttractorField* field){
  fieldName_lineEdit->setText(field->fieldName);
  EdgesList_lineEdit->setText(field->edgesList);
  FacesList_lineEdit->setText(field->facesList);
  FieldX_spinBox->setValue(field->fieldX);
  FieldY_spinBox->setValue(field->fieldY);
  FieldZ_spinBox->setValue(field->fieldZ);
  NNodesByEdge_spinBox->setValue(field->nNodesByEdge);
  NodesList_lineEdit->setText(field->nodeNumberString);
  bgField_checkBox->setChecked(field->backgroundField);
}

void FieldEditor::loadThresholdField(ThresholdField* field){
  fieldName_lineEdit->setText(field->fieldName);
  DistMax_spinBox->setValue(field->DistMax);
  DistMin_spinBox->setValue(field->DistMin);
  IField_spinBox->setValue(field->IField);
  LcMax_spinBox->setValue(field->LcMax);
  LcMin_spinBox->setValue(field->LcMin);
  sigmoid_checkBox->setChecked(field->Sigmoid);
  StopAtDistMax_checkBox->setChecked(field->StopAtDistMax);
  bgField_checkBox->setChecked(field->backgroundField);
}

///////////////////////////////////////////
// Field Saving
///////////////////////////////////////////

void FieldEditor::on_Save_Field_pushButton_clicked()
{
  bool savingNewField = (fieldSelectionBox->currentIndex() == 0);

  if (fieldTypeBox->currentText() == "Attractor"){
    AttractorField* newField = readAttractorFieldForm();
    savingNewField ? saveField(newField) : saveChanges(newField);
    std::cout << "Saved attractor" << std::endl;
  }
  else if (fieldTypeBox->currentText() == "Threshold") {
    ThresholdField* newField = readThresholdFieldForm();
    savingNewField ? saveField(newField) : saveChanges(newField);
    std::cout << "Saved Threshold" << std::endl;
  }
}

void FieldEditor::saveChanges(Field* savedField){
  fieldList[fieldSelectionBox->currentIndex()-1] = savedField;
  fieldSelectionBox->setItemText(fieldSelectionBox->currentIndex(), savedField->fieldName);
}

void FieldEditor::saveField(Field* newField){
  fieldList.push_back(newField);
  fieldSelectionBox->addItem(newField->fieldName);
}

AttractorField* FieldEditor::readAttractorFieldForm(){
  QString fieldName = fieldName_lineEdit->text();
  QString edgesList = EdgesList_lineEdit->text();
  QString facesList = FacesList_lineEdit->text();
  int fieldX = FieldX_spinBox->value();
  int fieldY = FieldY_spinBox->value();
  int fieldZ = FieldZ_spinBox->value();
  int nNodesByEdge = NNodesByEdge_spinBox->value();
  QString NodesList = NodesList_lineEdit->text();
  bool bgField = bgField_checkBox->isChecked();
  AttractorField* newfield = new AttractorField(fieldName, edgesList, facesList, fieldX, fieldY, fieldZ, nNodesByEdge, NodesList, bgField);
  return newfield;
}

ThresholdField* FieldEditor::readThresholdFieldForm(){
  QString fieldName = fieldName_lineEdit->text();
  int DistMax = DistMax_spinBox->value();
  int DistMin = DistMin_spinBox->value();
  int IField = IField_spinBox->value();
  int LcMax = LcMax_spinBox->value();
  int LcMin = LcMin_spinBox->value();
  bool Sigmoid = sigmoid_checkBox->isChecked();
  bool StopAtDistMax = StopAtDistMax_checkBox->isChecked();
  bool bgField = bgField_checkBox->isChecked();
  ThresholdField* newfield = new ThresholdField(fieldName, DistMax, DistMin, IField, LcMax, LcMin, Sigmoid, StopAtDistMax, bgField);
  return newfield;
}

