#ifndef FIELDEDITOR_H
#define FIELDEDITOR_H

#include <QWidget>

#include <QPushButton>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include <QFormLayout>

class Field;
class AttractorField;
class ThresholdField;

class FieldEditor: public QWidget
{
  Q_OBJECT

public:
  FieldEditor(QWidget* parent = 0);
  ~FieldEditor();

  std::vector<Field*> fieldList;

  ///////////////////////////////////////////
  // UI setup
  ///////////////////////////////////////////

  void setThresholdUI();
  void setAttractorUI();

private:

  ///////////////////////////////////////////
  // Top Layout
  ///////////////////////////////////////////

  QPushButton* newFieldButton = nullptr;
  QComboBox* fieldSelectionBox = nullptr;
  QComboBox* fieldTypeBox = nullptr;

  ///////////////////////////////////////////
  // Field Layout
  ///////////////////////////////////////////

  QPushButton* saveFieldButton = nullptr;
  QPushButton* deleteFieldButton = nullptr;


  ///////////////////////////////////////////
  // Field Layout
  ///////////////////////////////////////////

  QFormLayout *fieldLayout = nullptr;

  //Common elements
  QLineEdit* fieldName_lineEdit = nullptr;
  QCheckBox* bgField_checkBox = nullptr;

  //Attractor Field form
  QLineEdit* EdgesList_lineEdit = nullptr;
  QLineEdit* FacesList_lineEdit = nullptr;
  QSpinBox* FieldX_spinBox = nullptr;
  QSpinBox* FieldY_spinBox = nullptr;
  QSpinBox* FieldZ_spinBox = nullptr;
  QSpinBox* NNodesByEdge_spinBox= nullptr;
  QLineEdit* NodesList_lineEdit = nullptr;

  //Threshold Field form
  QSpinBox* DistMax_spinBox = nullptr;
  QSpinBox* DistMin_spinBox = nullptr;
  QSpinBox* IField_spinBox = nullptr;
  QSpinBox* LcMax_spinBox = nullptr;
  QSpinBox* LcMin_spinBox = nullptr;
  QCheckBox* sigmoid_checkBox = nullptr;
  QCheckBox* StopAtDistMax_checkBox = nullptr;


signals:

public slots:

  void on_New_Field_pushButton_clicked();

  ///////////////////////////////////////////
  // UI setup
  ///////////////////////////////////////////

  //Sets the UI to correspond to the type of field
  //with the name given by string
  void setUI(QString string);

  //Clears the field ui
  void clearUI();

  ///////////////////////////////////////////
  // Save Fields
  ///////////////////////////////////////////

  /*
   * Determines whether a new field or changes to an existing field are being saved
   * Then calls the appropriate function out of saveField() and saveChanges();
   */
  void on_Save_Field_pushButton_clicked();

  //Triggers adding a new entry to the fieldsList
  void saveField(Field* newField);
  //Triggers overwriting an existing entry in the fieldsList
  void saveChanges(Field* savedField);


  //Reads and constructs the field associated
  //with the values in the form for each type of field
  AttractorField* readAttractorFieldForm();
  ThresholdField* readThresholdFieldForm();

  ///////////////////////////////////////////
  // Load Fields
  ///////////////////////////////////////////

  /*
   * Determines the type of the Field at the given index
   * Then calls the appropriate function to load this Field
   */
  void loadField(int index);

  /*
   * Loads the given AttractorField's parameters into the UI
   */
  void loadAttractorField(AttractorField* field);

  /*
   * Loads the given ThresholdField's parameters into the UI
   */
  void loadThresholdField(ThresholdField* field);






};

#endif // FIELDEDITOR_H
