#ifndef PHYSICALGROUPSDIALOG_H
#define PHYSICALGROUPSDIALOG_H

#include <QDialog>
#include <QCheckBox>

class Mesh;

namespace Ui {
  class PhysicalGroupsDialog;
}

class PhysicalGroupsDialog : public QDialog
{
  Q_OBJECT

public:
  explicit PhysicalGroupsDialog(QWidget *parent = 0, int groupType=0, Mesh* mesh = nullptr);
  ~PhysicalGroupsDialog();

  void findPhysicalGroups(int groupType);
  void addGroupCheckbox(QString groupName);

  QString getNewGroupName();


  std::vector<QCheckBox*> groupCheckboxes;


private:
  Ui::PhysicalGroupsDialog *ui;

  Mesh* mesh = nullptr;

};

#endif // PHYSICALGROUPSDIALOG_H
