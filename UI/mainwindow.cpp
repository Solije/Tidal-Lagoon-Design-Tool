#include "UI/mainwindow.h"


#include <iostream>
#include <boost/filesystem.hpp>
#include <python2.7/Python.h>

#include <QFileDialog>
#include <QMouseEvent>
#include <QGraphicsLineItem>


#include "Mesh/vertex.h"
#include "Mesh/mesh.h"
#include "Mesh/spline.h"
#include "Mesh/Fields/field.h"
#include "UI/changelogtableview.h"
#include "vertexchangelog.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ui->statusBar->addPermanentWidget(ui->progressBar);
  ui->meshViewer->setScene(&scene);

  //Updating Mesh info in GUI
  connect(this, SIGNAL(meshSizeChanged(int)), ui->sideBar, SLOT(updateMeshInfo()));
  connect(this, SIGNAL(meshSizeChanged(int)), this, SLOT(updateMeshInfo()));
  connect(this, SIGNAL(activeVertexChanged(int)), ui->meshViewer , SLOT(setActiveVertex(int)));
  connect(this, SIGNAL(refreshVertexCoords(int)), ui->sideBar, SLOT(refreshVertexCoords(int)));

  connect(ui->meshViewer, SIGNAL(viewZoomed(double)), ui->sideBar, SLOT(updateZoomSpinbox(double)));

  // Rotating full view
  connect(this, SIGNAL(setViewRotation(int)), ui->meshViewer, SLOT(setViewRotation(int)));
  connect(ui->sideBar, SIGNAL(setViewRotation(int)), ui->meshViewer, SLOT(setViewRotation(int)));
  connect(this, SIGNAL(rotateView(int)), ui->meshViewer, SLOT(rotateView(int)));

  //Updating copy of mesh held by different widgets
  connect(ui->meshViewer, SIGNAL(newMeshLoaded(Mesh*)), ui->sideBar, SIGNAL(newMeshLoaded(Mesh*)));

  //Adding actions into changelog
  connect(this, SIGNAL(addVertexMovement(int)), ui->sideBar, SIGNAL(addVertexMovement(int)));
  connect(this, SIGNAL(addVertexInChangelog(int)), ui->sideBar, SIGNAL(addVertexInChangelog(int)));
  connect(this, SIGNAL(deleteVertexInChangelog(int)), ui->sideBar, SIGNAL(deleteVertexInChangelog(int)));
  connect(this, SIGNAL(addEdgeInChangelog(int, int)), ui->sideBar, SIGNAL(addEdgeInChangelog(int, int)));
  connect(this, SIGNAL(deleteEdgeInChangelog(int, int)), ui->sideBar, SIGNAL(deleteEdgeInChangelog(int, int)));

  //Changes Selection mode in meshViewer
  connect(ui->sideBar, SIGNAL(cycleViewMode()), ui->meshViewer, SLOT(cycleViewMode()));

  on_actionNew_Mesh_triggered();

}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::loadMeshFile(){
  if (ui->meshViewer->loadMesh()){
    Mesh* currentMesh = ui->meshViewer->getActiveMesh();

    emit meshSizeChanged(currentMesh->vertexCount());
    currentMesh = nullptr;
    updateMeshInfo();
  }
}

///////////////////////////////////////////
// Get functions
///////////////////////////////////////////

Mesh* MainWindow::getActiveMesh(){
  return ui->meshViewer->getActiveMesh();
}

FieldEditor* MainWindow::getFieldEditor(){
  return ui->sideBar->getFieldEditor();
}

MeshView* MainWindow::getMeshView(){
  return ui->meshViewer;
}

///////////////////////////////////////////
///////////////////////////////////////////
// Menubar slots
///////////////////////////////////////////
///////////////////////////////////////////

///////////////////////////////////////////
// File
///////////////////////////////////////////

void MainWindow::on_actionOpen_File_triggered()
{
    loadMeshFile();
}

void MainWindow::on_actionSave_File_triggered()
{
  QString filePath = QFileDialog::getSaveFileName(this, tr("Save Mesh File"), "~/" , tr("Geo Files (*.geo);;Mesh Files (*.msh)"));
  ui->meshViewer->getActiveMesh()->saveMesh(filePath);
}

///////////////////////////////////////////
// Edit
///////////////////////////////////////////

void MainWindow::on_actionInsert_mesh_triggered()
{
  ui->meshViewer->insertMesh();
}

void MainWindow::on_actionToggle_Sidebar_triggered()
{
    ui->sideBar->setVisible(!ui->sideBar->isVisible());
}

void MainWindow::on_actionRedo_triggered()
{
  ui->sideBar->redoChain();
}

void MainWindow::on_actionUndo_triggered()
{
  ui->sideBar->undoChain();
}

///////////////////////////////////////////
// View
///////////////////////////////////////////

void MainWindow::on_actionRotate_90_clockwise_triggered()
{
  emit rotateView(90);
}

void MainWindow::on_actionRotate_90_anticlockwise_triggered()
{
  emit rotateView(-90);
}

void MainWindow::on_actionReset_Rotation_triggered()
{
  emit setViewRotation(0);
}

///////////////////////////////////////////
// Status bar
///////////////////////////////////////////

void MainWindow::updateMeshInfo(){
  Mesh* currentMesh = ui->meshViewer->getActiveMesh();
  ui->statusBar->showMessage(tr(("Vertices: " + std::to_string(currentMesh->vertexCount()) +
                                " Lines: " + std::to_string(currentMesh->lineCount()) +
                                " Triangles: " + std::to_string(currentMesh->triCount())).c_str()));
  currentMesh = nullptr;
}

void MainWindow::updateProgressBar(double progress){
  ui->progressBar->setValue(progress);
}

///////////////////////////////////////////
// SideBar functions
///////////////////////////////////////////

QString MainWindow::getPrependText(){
  return ui->sideBar->getPrependText();
}

QString MainWindow::getAppendText(){
  return ui->sideBar->getAppendText();
}

void MainWindow::writeUnknownText(QString text){
  ui->sideBar->writeUnknownText(text);
}

void MainWindow::on_actionSave_Mesh_triggered(){
  QString filePath = QFileDialog::getSaveFileName(this, tr(""), "~/" , tr("Msh Files (*.msh)"));
  ui->meshViewer->getActiveMesh()->saveMeshAsGeo("./temp.geo");
  std::string gmshCmd = "gmsh ./temp.geo -2 -o " + filePath.toStdString();
  system(gmshCmd.c_str());
  boost::filesystem::remove("./temp.geo");

}

void MainWindow::on_actionRelax_Mesh_triggered()
{
  //This function has intermittent crashes for unknown reason
  //Likely to do with issues with importing numpy and python api more than once
  //Exmaples of similiar issues can be found online

  std::string currentFile = ui->meshViewer->getActiveMesh()->fileLocation;
  std::string tempFile = "/home/tom/temp.msh";
  std::string outputFile = "/home/tom/output_file.msh";
  ui->meshViewer->getActiveMesh()->saveMeshAsMsh(tempFile.c_str());

  // Initialize the Python interpreter.
  Py_Initialize();

  // Create some Python objects that will later be assigned values.
  PyObject *pName, *pModule, *pDict, *pFunc, *pArgs, *pstartMesh, *pdeformedMesh;
  std::cout << "declared PyObjects" << std::endl;
  // Convert the file name to a Python string.

  PyRun_SimpleString("import sys");
  PyRun_SimpleString("sys.path.append('/home/tom/Documents/Programming Projects/Force Equilibrium/')");
  std::cout << "imported sys" << std::endl;

  pName = PyString_FromString("relax_mesh");
  // Import the file as a Python module.
  std::cout << "about to import relax_mesh" << std::endl;
  pModule = PyImport_Import(pName);

  std::cout << (pModule == nullptr) << std::endl;
  // Create a dictionary for the contents of the module.
  pDict = PyModule_GetDict(pModule);

  // Get the add method from the dictionary.
  pFunc = PyDict_GetItemString(pDict, "relax");

  // Create a Python tuple to hold the arguments to the method.

  pArgs = PyTuple_New(2);

  // Convert 2 to a Python integer.

  pstartMesh = PyString_FromString(currentFile.c_str());
  pdeformedMesh = PyString_FromString(tempFile.c_str());

  // Set the Python int as the first and second arguments to the method.

  std::cout << "setting args" << std::endl;
  PyTuple_SetItem(pArgs, 0, pstartMesh);
  PyTuple_SetItem(pArgs, 1, pdeformedMesh);
  std::cout << "set args" << std::endl;

  // Call the function with the arguments.
  PyObject* pResult = PyObject_CallObject(pFunc, pArgs);
  std::cout << "called func" << std::endl;

  // Convert the result to a std::string from a Python object.
  //std::string result = PyString_AsString(pResult);

  Py_DECREF(pModule);
  Py_DECREF(pName);

  //Py_Finalize(0 is omitted
  //This is as it causes crashes on subsequent relaxations
  //Py_Finalize();

  boost::filesystem::copy_file(outputFile, currentFile, boost::filesystem::copy_option::overwrite_if_exists);
  ui->meshViewer->loadMesh(currentFile);

}

void MainWindow::on_actionNew_Mesh_triggered()
{
  getMeshView()->loadEmptyMesh();
}
