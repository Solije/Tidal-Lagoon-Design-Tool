#ifndef VERTEXCHANGELOG_H
#define VERTEXCHANGELOG_H

#include <QAbstractTableModel>
#include "movement.h"


class VertexChangelog : public QAbstractTableModel
{
    Q_OBJECT
public:
    VertexChangelog(QObject *parent = 0);

    ///////////////////////////////////////////
    // QAbstractTabeModel Functions
    ///////////////////////////////////////////

    /*
    * Returns the number of rows in the table
    * @param QModelIndex parent: unused parameter
    * @return int: number of columns in the model
    */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /*
    * Returns the number of columns in the table
    * @param QModelIndex parent: unused parameter
    * @return int: number of rows in the model
    */
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    /*
    * Returns the data contained in the table at a given index
    * @param QModelIndex index: index of the cell which is being read with (0,0) being the top left cell
    * @param int role: Which characteristic of the cell to return. eg. text, font, background colour, etc.
    * @return QVariant: The specified characteristic of the chosen cell.
    */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;


    /*
     * Sets the titles of the columns
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    ///////////////////////////////////////////
    // Extra Functions
    ///////////////////////////////////////////

    /*
     * Helper function to append a row to the table and fill it with a Movement
     */
    bool addMovement(Movement newMovement, const QModelIndex &index);

    /*
     * Returns the Movement held at given index in listOfMovements
     */
    Movement retrieveMovement(int index);

    /*
     * Removes all Movements from the table.
     */
    void clearList();

private:

    /*
     * List of recorded Movements
     * This forms the history of the mesh
     *
     * By tranversing back and forth along this QList, undo/redo functionality is possible
     */
    QList<Movement> listOfMovements;
};

#endif // VERTEXCHANGELOG_H
