#include "vertexchangelog.h"
#include <iostream>
#include <string>
#include "movement.h"

VertexChangelog::VertexChangelog(QObject *parent)
    :QAbstractTableModel(parent)
{
}



///////////////////////////////////////////
// Standard QAbstractModel methods
///////////////////////////////////////////

int VertexChangelog::rowCount(const QModelIndex & /*parent*/) const
{
 return listOfMovements.size();
}


int VertexChangelog::columnCount(const QModelIndex & /*parent*/) const
{
  return 3;
}


QVariant VertexChangelog::data(const QModelIndex &index, int role) const
{
  int col = index.column();
  int row = index.row();
  Movement action = listOfMovements.value(row);
  int actionType = action.getActionType();
  switch (role){
  case Qt::DisplayRole:
    if (col == 0){
      switch(actionType){
      case Movement::vertexMovement:
      case Movement::addVertex:
      case Movement::deleteVertex:{
        QString ID = std::to_string(listOfMovements.value(row).vertexNumber).c_str();
        return ID;
      }
      case Movement::addEdge:
      case Movement::deleteEdge:{
          QString ID = "Edge";
          return ID;
      }
      }
    } else if (col == 1){
      switch (actionType){
      case Movement::vertexMovement:
      case Movement::addVertex:
      case Movement::deleteVertex:{
        //old position
        QPointF oldPosition = action.oldPos;
        QString oldCoords = ("(" + std::to_string(oldPosition.x()) + "," + std::to_string(oldPosition.y()) + ")").c_str();
        return oldCoords;
      }
      case Movement::addEdge:
      case Movement::deleteEdge:{
          QString endPoints = (std::to_string(action.startVertexNumber) + " to " + std::to_string(action.endVertexNumber)).c_str();
          return endPoints;
      }
      }
    } else if(col == 2){
      switch (actionType){
      case Movement::vertexMovement:{
        //new position
        QPointF newPosition = listOfMovements.value(row).newPos;
        QString newCoords = ("(" + std::to_string(newPosition.x()) + "," + std::to_string(newPosition.y()) + ")").c_str();
        return newCoords;
      }
      case Movement::addVertex:
      case Movement::addEdge:
        return "Addition";
      case Movement::deleteVertex:
      case Movement::deleteEdge:
        return "Deletion";
      }
    } else{
      //nothing
    }
    break;
   }

  return QVariant();
}


QVariant VertexChangelog::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("ID");

            case 1:
                return tr("Start");

            case 2:
              return tr("End");

            default:
                return QVariant();
        }
    }
    return QVariant();
}


///////////////////////////////////////////
// Custom Data handling methods
///////////////////////////////////////////

bool VertexChangelog::addMovement(Movement newMovement, const QModelIndex &index){
  Q_UNUSED(index);

  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  listOfMovements.append(newMovement);
  endInsertRows();
  return true;
}


void VertexChangelog::clearList(){
  beginResetModel();
  listOfMovements.clear();
  endResetModel();
}


Movement VertexChangelog::retrieveMovement(int index){
  return listOfMovements.value(index);
}
