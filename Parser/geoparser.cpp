#include "Parser/geoparser.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string.hpp>

#include <memory>

#include "UI/mainwindow.h"
#include "UI/fieldeditor.h"
#include "Mesh/mesh.h"
#include "Mesh/vertex.h"
#include "Mesh/Fields/field.h"
#include "Mesh/Fields/attractorfield.h"
#include "Mesh/Fields/thresholdfield.h"

GeoParser::GeoParser()
{

}

///////////////////////////////////////////
// Public Functions
///////////////////////////////////////////

Mesh* GeoParser::parse(std::string geoFile, MeshView* view){
  parentView = view;
  //std::shared_ptr<Mesh> newMesh(new Mesh(view));
  Mesh* newMesh = new Mesh(view);
  newMesh->meshType = Mesh::geo;
  return parseFile(geoFile, newMesh);
}

Mesh* GeoParser::insertMesh(std::string geoFile, Mesh* existingMesh){
  parentView = qobject_cast<MeshView*>(existingMesh->parent());
  initialMeshSize = existingMesh->vertexCount();

  return parseFile(geoFile,existingMesh);

}

Mesh* GeoParser::parseFile(std::string geoFile, Mesh* mesh){

  buildVertexMap(geoFile);

  std::ifstream file(geoFile.c_str());
  if (!file){
    std::cout << "Couldn't open .msh file: " << geoFile.c_str() << std::endl;
    return nullptr;
  }

  std::cout << "File loaded successfully" << std::endl;

  std::string line;
  QString unknownText;
  while(std::getline(file, line)){
    std::cout << line << std::endl;

    if (line.find("Field") != std::string::npos){
      readField(file, line, mesh);
    }
    else if (line.find("Physical Point") != std::string::npos){
      readPhysicalGroup(line, mesh);
    }
    else if (line.find("Physical Line") != std::string::npos){
      readPhysicalGroup(line, mesh);
    }
    //represents a vertex
    else if (line.find("Point") != std::string::npos){
      readPoint(line, mesh);
    }
    //Represents a line
    else if (line.find("BSpline") != std::string::npos){
        readSpline(line, mesh);
    }

    else if (line.find("Line") != std::string::npos){
      readLine(line, mesh);
    }
    else{
      unknownText.append((line + '\n').c_str());
    }
  }
  writeUnknownText(unknownText);
  return mesh;
}

///////////////////////////////////////////
///////////////////////////////////////////
// Parsing functions
///////////////////////////////////////////
///////////////////////////////////////////

///////////////////////////////////////////
// Vertices
///////////////////////////////////////////

void GeoParser::writeUnknownText(QString text){
  qobject_cast<MainWindow*>(parentView->window())->writeUnknownText(text);
}


void GeoParser::readPoint(std::string line, Mesh* newMesh){

  int vertexNumber = newMesh->vertexCount() + 1;
  std::size_t paraOpen = line.find("{");
  std::size_t paraClosed = line.find("}");
  std::string pointInfo= line.substr(paraOpen+1,(paraClosed-paraOpen-1));

  std::vector<std::string> pointData;
  boost::algorithm::split(pointData, pointInfo, boost::is_any_of(","));
  //Data consisting of:
  // [0] - x coordinate
  // [1] - y coordinate
  // [2] - z coordinate
  // and others

  //print
  std::cout << "Vertex " << vertexNumber << " (" << pointData[0] << "," << pointData[1] << "," << pointData[2] << ")" << std::endl;
  //Add vertex to Mesh object
  //Start index from 1 as [0] is the vertex index
  Vertex* newVertex = new Vertex(vertexNumber, std::stod(pointData[0]), std::stod(pointData[1]), std::stod(pointData[2]), newMesh);

  //trackChange = false
  newMesh->addVertex(newVertex, false);
}

///////////////////////////////////////////
// Lines/Splines
///////////////////////////////////////////

void GeoParser::readLine(std::string line, Mesh* newMesh){

  std::vector<Vertex*> vertexPath = readExpressionList(line, newMesh);
  for (unsigned int i = 0; i < vertexPath.size()-1; i++){
      newMesh->addLine(vertexPath[i], vertexPath[i+1], false);
  }

  //If line is made up of more than one section, make it a LineGroup
  if (vertexPath.size() > 2){
    newMesh->addLineGroup(vertexPath);
  }
}


void GeoParser::readSpline(std::string line, Mesh* newMesh){

  std::vector<Vertex*> vertexPath = readExpressionList(line, newMesh);
  for (unsigned int i = 0; i < vertexPath.size()-1; i++){
      newMesh->addLine(vertexPath[i], vertexPath[i+1], false);
  }

  //Adds the spline to the mesh
  newMesh->addSpline(vertexPath);
}

///////////////////////////////////////////
// Physical Groups
///////////////////////////////////////////

void GeoParser::readPhysicalGroup(std::string line, Mesh* newMesh){
  std::size_t paraOpen = line.find("(");
  std::size_t paraClosed = line.find(")");
  QString groupName = QString::fromStdString(line.substr(paraOpen+1,(paraClosed-paraOpen-1)));
  //If group has a string identifier need to strip quotes
  if (groupName.left(1) == QString('"')) {
      groupName.remove( 0, 1 );
      groupName.chop(1);
  }
  std::vector<Vertex*> groupElements = readExpressionList(line, newMesh);
  for (Vertex* item : groupElements){
      newMesh->addToPhysGroup(item, groupName);
  }
}

///////////////////////////////////////////
// Fields
///////////////////////////////////////////

void GeoParser::readField(std::ifstream &file, std::string line, Mesh* newMesh){
  if (line.find("Threshold") != std::string::npos){
    std::vector<int> fieldParameters;
    for (int i = 0; i <= 6; i++){
      std::getline(file, line);
      std::size_t paraOpen = line.find("=");
      std::size_t paraClosed = line.find("}");
      std::string pointInfo= line.substr(paraOpen+1,(paraClosed-paraOpen-1));
      fieldParameters.push_back(std::stoi(pointInfo));
    }
    QString fieldName = "Loaded field " + QString::number(newMesh->fieldEditor->fieldList.size()+1);
    ThresholdField* newField = new ThresholdField(fieldName, fieldParameters[0],fieldParameters[1],fieldParameters[2],fieldParameters[3],fieldParameters[4], fieldParameters[5], fieldParameters[6], false);
    newMesh->fieldEditor->saveField(newField);
  }
  else if (line.find("Attractor") != std::string::npos){
    std::vector<QString> fieldParameters;
    for (int i = 0; i <= 6; i++){
      std::getline(file, line);
      std::size_t startPos;
      std::size_t endPos;
      if (line.find("{") != std::string::npos){
        startPos = line.find("{");
        endPos = line.find("}");
      }
      else {
        startPos = line.find("=");
        endPos = line.find(";");
      }

      std::string pointInfo = line.substr(startPos+1,(endPos-startPos-1));
      fieldParameters.push_back(QString(pointInfo.c_str()));
    }
    QString fieldName = "Loaded field " + QString::number(newMesh->fieldEditor->fieldList.size()+1);
    AttractorField* newField = new AttractorField(fieldName, fieldParameters[0],fieldParameters[1], fieldParameters[2].toInt(), fieldParameters[3].toInt(),
                                                  fieldParameters[4].toInt(), fieldParameters[5].toInt() ,fieldParameters[6], false);
    newMesh->fieldEditor->saveField(newField);
  }
}


///////////////////////////////////////////
///////////////////////////////////////////
// Helper functions
///////////////////////////////////////////
///////////////////////////////////////////

std::vector<Vertex*> GeoParser::readExpressionList(std::string line, Mesh* newMesh){

  std::size_t paraOpen = line.find("{");
  std::size_t paraClosed = line.find("}");
  std::string splineInfo= line.substr(paraOpen+1,(paraClosed-paraOpen-1));

  std::vector<std::string> lineData;
  boost::algorithm::split(lineData, splineInfo, boost::is_any_of(","));
  //Now have a vector of either single points or ranges of points denoted by a 2 points with a colon between

  std::vector<Vertex*> vertexPath;
  for (std::string expression : lineData){
    //Remove extra whitespace from ID key as will prevent matching
    boost::trim(expression);

//    std::cout << expression << std::endl;

    // If expression is a single point
    if (expression.find(":") == std::string::npos) {

      //Find the identfier of vertex in mapping to find vertexNumber
      std::vector<std::string>::iterator startIter = std::find(vertexMap.begin(), vertexMap.end(), expression);
      int vertexIndex = startIter - vertexMap.begin() + initialMeshSize;
      vertexPath.push_back(newMesh->getVertexFromIndex(vertexIndex));
    }
    else {
      //expression is a range of points
      std::vector<std::string> vertexRange;
      boost::algorithm::split(vertexRange, expression, boost::is_any_of(":"));

      std::string vertex1 = vertexRange[0];
      boost::trim(vertex1);
      std::string vertex2 = vertexRange[1];
      boost::trim(vertex2);

      std::vector<std::string>::iterator startIter = std::find(vertexMap.begin(), vertexMap.end(), vertex1);
      int vertex1Index = startIter - vertexMap.begin() + initialMeshSize;
      std::vector<std::string>::iterator endIter = std::find(vertexMap.begin(), vertexMap.end(), vertex2);
      int vertex2Index = endIter - vertexMap.begin() + initialMeshSize;


      while (vertex1Index <= vertex2Index ){
//        std::cout << "joining " << vertex1Index + 1 << " to " << vertex1Index  + 2 << std::endl;
        vertexPath.push_back(newMesh->getVertexFromIndex(vertex1Index));
        vertex1Index++;
      }


    }
  }

  return vertexPath;
}


bool GeoParser::buildVertexMap(std::string geoFile){
  std::ifstream file(geoFile.c_str());

  if (!file){
    std::cout << "Couldn't open .geo file: " << geoFile.c_str() << std::endl;
    return false;
  }
  std::cout << "Building Vertex Map" << std::endl;

  std::string line;

  //Throw away first line
  //May need for future when saving it but not yet
  //cl__1 is the size of the mesh elements at a given point
  std::getline(file, line);
  while(std::getline(file, line)){
    //represents a vertex
    if (line.find("Point") != std::string::npos && line.find("Physical Point") == std::string::npos){
      std::size_t braClosed = line.find(")");
      std::size_t braOpen = line.find("(");
      //index jimmying to remove brackets
      std::string index = line.substr(braOpen+1,(braClosed-braOpen-1));
      vertexMap.push_back(index);
    }
  }

  std::cout << "Successfully built Vertex Map" << std::endl;

  return true;
}
