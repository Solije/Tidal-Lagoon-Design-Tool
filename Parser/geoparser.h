#ifndef GEOPARSER_H
#define GEOPARSER_H

#include <string>
#include <vector>

#include <QString>

class Mesh;
class Vertex;
class MeshView;

class GeoParser
{
public:
  GeoParser();

  /*
   * Main function
   * Reads the given geo file and returns a representation of it as a Mesh object
   */
  Mesh* parse(std::string geoFile, MeshView* view);

  /*
   * Main function
   * Reads the given geo file and inserts the contructed Mesh into the given existing mesh
   */
  Mesh* insertMesh(std::string geoFile, Mesh* existingMesh);

 /*
  * Helper function
  * Contains the logic for parsing geo file
  * Adds the contents of the file to the given mesh
  */
  Mesh* parseFile(std::string geoFile, Mesh* mesh);


private:

  ///////////////////////////////////////////
  // Parsers for various Mesh Elements
  ///////////////////////////////////////////

  /*
   * Reads in a point from the file and constructs an appropriate Vertex
   */
  void readPoint(std::string line, Mesh* newMesh);

  /*
   * Reads in a line from the file and constructs MeshEdges between the appropriate Vertices
   */
  void readLine(std::string line, Mesh* newMesh);

  /*
   * Reads in a spline from the file, constructs required MeshEdges and their parent Spline.
   */
  void readSpline(std::string line, Mesh* newMesh);

  void readPhysicalGroup(std::string line, Mesh* newMesh);

  void readField(std::ifstream &file, std::string line, Mesh* newMesh);

  ///////////////////////////////////////////
  // Helper functions
  ///////////////////////////////////////////

  /*
   * Reads a list of point IDs and constructs a vector of the corresponding vertices
   */
  std::vector<Vertex*> readExpressionList(std::string line, Mesh* newMesh);

  /*
   * Constructs the vector vertexMap
   */
  bool buildVertexMap(std::string geoFile);

  /*
   * Writes out the unknown lines (which will include fields, etc) to Append_Text_textEdit in SideBar
   */
  void writeUnknownText(QString text);

  //Vector of geofile's point IDs
  //Allows line elements to use previously defined points
  //This is as this vector has the same ordering as the Mesh's vertex vector
  std::vector<std::string> vertexMap;

  MeshView* parentView = nullptr;

  int initialMeshSize = 0;
};

#endif // GEOPARSER_H
