#include "Parser/mshparser.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>


#include "Mesh/mesh.h"
#include "Mesh/vertex.h"

MshParser::MshParser()
{

}

Mesh* MshParser::parse(std::string meshFile, MeshView* view){
  Mesh* newMesh = new Mesh(view);
  newMesh->meshType = Mesh::msh;
  newMesh->fileLocation = meshFile;
  std::ifstream file(meshFile.c_str());

  if (!file){
    std::cout << "Couldn't open .msh file: " << meshFile.c_str() << std::endl;
    return nullptr;
  }
  std::cout << "File loaded successfully" << std::endl;

  std::string line;
  std::vector<std::vector<int> > lines;
  std::vector<std::vector<int> > triLines;
  while(std::getline(file, line)){
    if (line == "$Nodes"){
      std::cout << "Reading vertices from file" << std::endl;

      //Read number of nodes from next line and store as int
      std::getline(file, line);
      int noOfNodes = std::stoi(line);
      std::cout << "Number of nodes is " << noOfNodes <<std::endl;

      //Read lines until reaching $EndNodes
      //Each line is a vertex
      int i = 0;
      while (std::getline(file, line) && line != "$EndNodes"){
//      emit updateProgressBar(double(i)/noOfNodes * 100);
        i++;
        std::vector<std::string> vertexData;
        boost::algorithm::split(vertexData, line, boost::is_any_of(" "));

        //Add vertex to Mesh object
        //Start index from 1 as [0] is the vertex index
        Vertex* newVertex = new Vertex(i, std::stod(vertexData[1]), std::stod(vertexData[2]), std::stod(vertexData[3]), newMesh);
        newMesh->addVertex(newVertex, false);
      }
      std::cout << "Finished reading vertices" << std::endl;
      }
    if (line == "$Elements"){

        //Read number of Elements from next line and store as int
        std::getline(file, line);
        int noOfElements = std::stoi(line);
        std::cout << "Number of Elements is " << noOfElements <<std::endl;
        newMesh->elementsText.append(line + "\n");
        int i = 0;
        //Start and end vertices of lines in the mesh


        while (std::getline(file, line) && line != "$EndElements"){
//          emit updateProgressBar(double(i)/noOfElements * 100);
          i++;
          //Read line
          std::vector<std::string> elementData;
          boost::algorithm::split(elementData, line, boost::is_any_of(" "));

          switch (std::stoi(elementData[1])){

            case 15: // 15 represents an external vertex
              //Add in logic such that these are shown differently?
              break;

            case 1: // 1 represents a line between two vertices
              {
              /* Shouldn't be passed to Mesh.lines as triangles give all connections between vertices, would only add dupes
               * Important to keep for rebuilding the .msh file
               */
              //Last two ints on each line represent the two vertices
              int vertex1 = std::stoi(elementData[elementData.size()-2]);
              int vertex2 = std::stoi(elementData[elementData.size()-1]);
              lines.push_back(std::vector<int>{std::min(vertex1,vertex2),std::max(vertex1,vertex2)});
              break;
              }
            case 2: // 2 represents a triangle between 3 vertices
              {     // Hence will gain 3 lines per entry with duplicates
              int vertex1 = std::stoi(elementData[elementData.size()-3]);
              int vertex2 = std::stoi(elementData[elementData.size()-2]);
              int vertex3 = std::stoi(elementData[elementData.size()-1]);
              triLines.push_back(std::vector<int>{std::min(vertex1,vertex2),std::max(vertex1,vertex2)});
              triLines.push_back(std::vector<int>{std::min(vertex1,vertex3),std::max(vertex1,vertex3)});
              triLines.push_back(std::vector<int>{std::min(vertex2,vertex3),std::max(vertex2,vertex3)});
              //Duplicate lines are added here
              break;
              }
          }
          newMesh->elementsText.append(line + "\n");
        }

      }
  }
    std::cout << "end of Elements" << std::endl;
    //If possible, construct lines from mesh triangles
    if (triLines.size() > 0){
      std::cout << "Reading lines from triangle data" << std::endl;
      //Sorts and removes duplicates from triLines vector
      std::sort(triLines.begin(), triLines.end(), sortRule);
      triLines.erase(std::unique(triLines.begin(), triLines.end()), triLines.end());
      //Creates MeshEdges objects
      for (std::vector<int> p: triLines){
          newMesh->addLine(p[0], p[1], false);
        }
    } else {
      //If not possible, read explicitly mentioned lines
        std::cout << "Reading lines from line data" << std::endl;
        for (std::vector<int> p: lines){
           newMesh->addLine(p[0], p[1], false);
        }
      }
  std::cout << "Successfully loaded mesh" << std::endl;
  return newMesh;
}
