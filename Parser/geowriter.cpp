#include "Parser/geowriter.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

#include "Mesh/mesh.h"
#include "Mesh/Fields/field.h"
#include "Mesh/Fields/attractorfield.h"
#include "Mesh/Fields/thresholdfield.h"
#include "UI/mainwindow.h"
#include "UI/fieldeditor.h"

GeoWriter::GeoWriter()
{

}

GeoWriter::~GeoWriter(){
  delete file;
}

bool GeoWriter::write(std::string savePath, Mesh* geoMesh){
  geoMesh->collapseVertexVector();
  buildVertexMap(geoMesh);
  file = new std::ofstream(savePath.c_str());
  if (!file->is_open()){
    std::cout << "couldn't write to file" << std::endl;
    return false;
  }
  else{
    lineIndex = 1;

    printPrependedText(geoMesh);
    *file << "cl__1 = 0.1;\nIP=newp;\nIL=newl;\nIS=news;\nIF=newf;\n";
    for (Spline* spline : geoMesh->splineList){
      printSpline(spline);
    }

    for (Spline* lineGroup : geoMesh->lineGroupList){
      printLineGroup(lineGroup);
    }

    for (Vertex* vert : geoMesh->getVertices()){
      if(vert->parentSplines.empty()){
        printVertex(vert);
      }
    }

    for (std::shared_ptr<MeshEdge> line : geoMesh->getLines()){
      if(line->parentSpline == nullptr){
        //Only add lines if they haven't already been used to make a spline
        printLine(line);
      }
    }

    printSurfaces(geoMesh);
    printPhysicalGroups(geoMesh);

    printFields(geoMesh->fieldEditor->fieldList);
    printAppendedText(geoMesh);

    /*
     * Line loops are unnecessary for now.
    std::cout << "Now looking for loops" << std::endl;
    for (std::vector<int> loop : findLoops(geoMesh)){
      std::string lineEntry = "Line Loop(10) = {";
      for (int line : loop){
        lineEntry.append(std::to_string(line)+ ", ");
      }
      lineEntry.pop_back(); //pop " "
      lineEntry.pop_back(); //pop ","
      lineEntry.append("};\n");
      file << lineEntry;
    }
    */
    file->close();
    return true;
  }
}

///////////////////////////////////////////
// Freeform text
///////////////////////////////////////////

void GeoWriter::printPrependedText(Mesh* geoMesh){
  QString prependText = qobject_cast<MainWindow*>(qobject_cast<MeshView*>(geoMesh->parent())->window())->getPrependText();
  if (!prependText.isEmpty()){
    *file << prependText.toStdString() << "\n" ;
  }
}

void GeoWriter::printAppendedText(Mesh* geoMesh){
  QString appendText = qobject_cast<MainWindow*>(qobject_cast<MeshView*>(geoMesh->parent())->window())->getAppendText();
  if (!appendText.isEmpty()){
    *file << appendText.toStdString() << "\n" ;
  }
}

///////////////////////////////////////////
// Points
///////////////////////////////////////////

void GeoWriter::printVertex(Vertex* savedVertex){
  std::vector<int>::iterator startIter = std::find(vertexMap.begin(), vertexMap.end(), savedVertex->vertexNumber);
  std::string ID = QString::number(startIter - vertexMap.begin() + 1).toStdString();
  std::string xPos = std::to_string(savedVertex->x());
  std::string yPos = std::to_string(savedVertex->y());
  std::string zPos = std::to_string(savedVertex->getZPos());
  //CHANGE WHEN THIS PROPERTY IS HELD ON VERTEX
  std::string meshSizeParam = std::to_string(0.1);
  std::string vertexLine = "Point(IP + " + ID + ") = {" + xPos + "," + yPos + "," + zPos /*+ "," + meshSizeParam*/ + "};\n";
  *file << vertexLine;
  savedVertices.push_back(savedVertex->vertexNumber);
}

///////////////////////////////////////////
// Lines/Splines
///////////////////////////////////////////

void GeoWriter::printLine(std::shared_ptr<MeshEdge> savedLine){
  std::string ID = std::to_string(lineIndex);
  std::vector<int>::iterator startIter = std::find(savedVertices.begin(), savedVertices.end(), savedLine->startPoint->vertexNumber);
  std::string startPoint = std::to_string(startIter - savedVertices.begin() + 1);
  std::vector<int>::iterator endIter = std::find(savedVertices.begin(), savedVertices.end(), savedLine->endPoint->vertexNumber);
  std::string endPoint = std::to_string(endIter - savedVertices.begin() + 1);

  std::string lineLine = "Line(IL + " + ID + ") = {IP + " + startPoint + ", IP + " + endPoint + "};\n";
  *file << lineLine;
  lineIndex++;
  lineIDList.push_back(savedLine.get());

}

void GeoWriter::printSpline(Spline* savedSpline){
  std::vector<Vertex*> exportVertices = savedSpline->verticesPath;
  if (savedSpline->closedLoop){
    exportVertices.pop_back();
  }
  for (Vertex* vert : exportVertices){
    if (std::find(savedVertices.begin(), savedVertices.end(), vert->vertexNumber) == savedVertices.end()){
      printVertex(vert);
    }
  }
  QString splineString;
  splineString = "BSpline(IL + " + QString::number(lineIndex) + ") = {"+ buildExpressionList(savedSpline->verticesPath)+"};\n";
  *file << splineString.toStdString();
  lineIndex++;
  lineIDList.push_back(savedSpline);
}

void GeoWriter::printLineGroup(Spline* savedLineGroup){
  std::vector<Vertex*> exportVertices = savedLineGroup->verticesPath;
  if (savedLineGroup->closedLoop){
    exportVertices.pop_back();
  }
  for (Vertex* vert : exportVertices){
    if (std::find(savedVertices.begin(), savedVertices.end(), vert->vertexNumber) == savedVertices.end()){
      printVertex(vert);
    }
  }
  QString splineString;
  splineString = "Line(IL + " + QString::number(lineIndex) + ") = {"+ buildExpressionList(savedLineGroup->verticesPath)+"};\n";
  *file << splineString.toStdString();
  lineIndex++;
  lineIDList.push_back(savedLineGroup);


}

///////////////////////////////////////////
// Line Loops and Surfaces
///////////////////////////////////////////

QString GeoWriter::printSurfaces(Mesh* geoMesh){
  //Find coast in lagoon
  auto it = std::find_if(geoMesh->physGroups.begin(), geoMesh->physGroups.end(), [](const PhysicalGroup* obj) {return obj->groupName == "Lagoon";});
  if (it != geoMesh->physGroups.end()){
    *file << findLoop(**it).toStdString();
    *file << "Plane Surface(" + QString::number(LineLoopIndex - 1).toStdString() + ") = {" + QString::number(LineLoopIndex - 1).toStdString() + "};\n";
    *file << "Physical Surface(" + QString::number(LineLoopIndex - 1).toStdString() + ") = {" + QString::number(LineLoopIndex - 1).toStdString() + "};\n";
    std::cout << "I found the Lagoon physical Group!!!!!!" << std::endl;
  }
  it = std::find_if(geoMesh->physGroups.begin(), geoMesh->physGroups.end(), [](const PhysicalGroup* obj) {return obj->groupName == "Ocean";});
  if (it != geoMesh->physGroups.end()){
    *file << findLoop(**it).toStdString();
    *file << "Plane Surface("+QString::number(LineLoopIndex - 1).toStdString()+") = {" + QString::number(LineLoopIndex - 1).toStdString() + "};\n";
    *file << "Physical Surface(" + QString::number(LineLoopIndex - 1).toStdString() + ") = {" + QString::number(LineLoopIndex - 1).toStdString() + "};\n";
    std::cout << "I found the Ocean physical Group!!!!!!" << std::endl;
  }

  return QString();
}

QString GeoWriter::findLoop(PhysicalGroup loopGroup){
  std::vector<LineSection*> lineLoop;
  std::vector<bool> reverseDirection;
  MeshElement* currentElement = loopGroup.elementList.front();
  LineSection* line = dynamic_cast<LineSection*>(currentElement);
  lineLoop.push_back(line);
  reverseDirection.push_back(false);
  Vertex* startVertex = line->startVertex();
  Vertex* currentVertex = line->endVertex();

  while (currentVertex != startVertex){
    //Find splines attached to vertex
    std::vector<Spline*> vertexSplines = currentVertex->parentSplines;
    //Remove the previous spline to prevent backtracking
    vertexSplines.erase(std::remove(vertexSplines.begin(), vertexSplines.end(), currentElement), vertexSplines.end());

    if (vertexSplines.size() == 1){
      //simple case. one spline in, one spline out

      //Move to new spline
      currentElement = vertexSplines.front();
      LineSection* currentLine = dynamic_cast<LineSection*>(currentElement);
      //Add new spline to loop
      lineLoop.push_back(currentLine);

      //Find vertex on other end of spline
      if (currentLine->startVertex() == currentVertex){
        currentVertex = currentLine->endVertex();
        //loop runs from start of spline to end
        reverseDirection.push_back(false);
      }
      else{
        currentVertex = currentLine->startVertex();
        //loop runs from end of spline to front
        reverseDirection.push_back(true);
      }
    }

  }

  QString loopEntry = "Line Loop(" + QString::number(LineLoopIndex) + ") = {";
  for (std::pair<std::vector<LineSection*>::iterator, std::vector<bool>::iterator> i(lineLoop.begin(), reverseDirection.begin());
       i.first != lineLoop.end() /* && i.second != dubVec.end() */; ++i.first, ++i.second)
  {
    loopEntry.append(((*i.second) ? "-" : ""));
    std::vector<MeshElement*>::iterator startIter = std::find(elementMap.begin(), elementMap.end(), *i.first);
    QString elementIndex = QString::number(startIter - elementMap.begin() + 1);
    loopEntry.append("(IL + " + elementIndex + "), ");
  }
  loopEntry.chop(2);
  loopEntry.append("};\n");
  LineLoopIndex++;
  return loopEntry;
}

///////////////////////////////////////////
// Physical Groups
///////////////////////////////////////////

void GeoWriter::printPhysicalGroups(Mesh* geoMesh){
  for (PhysicalGroup* group : geoMesh->physGroups){
    QString groupString;

    //Define what kind of group this is and add prefix
    switch (group->elementType){
    case MeshElement::Point:
      groupString.append("Physical Point");
      break;

    case MeshElement::Line:
      groupString.append("Physical Line");
      break;
    }
    groupString.append("(\"" + group->groupName + "\") = {");

    for (MeshElement* element : group->elementList){
      //Searches for Element in list of Elements to find identifier.
      //Needs to be fixed to point to consistent list across all elements
      //Currently just looking at elements within the group

      //Depending on the type of elements print out the full identifiers
      switch (group->elementType){
      case MeshElement::Point:
        {
        std::vector<int>::iterator startIter = std::find(vertexMap.begin(), vertexMap.end(), dynamic_cast<Vertex*>(element)->vertexNumber);
        QString elementIndex = QString::number(startIter - vertexMap.begin() + 1);
        groupString.append("IP + " + elementIndex + ", ");
        break;
        }
      case MeshElement::Line:
        std::vector<MeshElement*>::iterator startIter = std::find(elementMap.begin(), elementMap.end(), element);
        QString elementIndex = QString::number(startIter - elementMap.begin() + 1);
        groupString.append("IL + " + elementIndex + ", ");
        break;
      }
    }
    //Remove trailing ", " from string and add terminating chars
    groupString.chop(2);
    groupString.append("};\n");

//    std::cout << groupString.toStdString() << std::endl;
    *file << groupString.toStdString();
  }
}

///////////////////////////////////////////
// Fields
///////////////////////////////////////////

void GeoWriter::printFields(std::vector<Field*> fieldList){
  for (int fieldIndex = 0; fieldIndex < fieldList.size(); fieldIndex++){
    *file << fieldList[fieldIndex]->printFieldInfo(fieldIndex+1, this).toStdString();
  }
}

///////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////

void GeoWriter::buildVertexMap(Mesh* geoMesh){

  for (Spline* spline : geoMesh->splineList){
      elementMap.push_back(spline);
      std::vector<Vertex*> exportVertices = spline->verticesPath;
      if (spline->closedLoop){
        exportVertices.pop_back();
      }
      for (Vertex* vert : exportVertices){
        if (std::find(vertexMap.begin(), vertexMap.end(), vert->vertexNumber) == vertexMap.end()){
          vertexMap.push_back(vert->vertexNumber);
        }
      }
  }

  for (Spline* lineGroup : geoMesh->lineGroupList){
      elementMap.push_back(lineGroup);
      std::vector<Vertex*> exportVertices = lineGroup->verticesPath;
      if (lineGroup->closedLoop){
        exportVertices.pop_back();
      }
      for (Vertex* vert : exportVertices){
        if (std::find(vertexMap.begin(), vertexMap.end(), vert->vertexNumber) == vertexMap.end()){
          vertexMap.push_back(vert->vertexNumber);
        }
      }
  }

  for (Vertex* vert : geoMesh->getVertices()){
    if(vert->parentSplines.empty()){
      vertexMap.push_back(vert->vertexNumber);
    }
  }

  std::cout << "printing vertexMap:" << std::endl;
  for(int vert : vertexMap){
    std::cout << vert << std::endl;
  }
}

QString GeoWriter::buildExpressionList(std::vector<Vertex*> vertexPath){
  QString expressionString;
  for (unsigned int index = 0; index < vertexPath.size(); index++){
    int lookAhead = 1;
    while ((index + lookAhead) < vertexPath.size() && mapVertexToFileIndex(vertexPath[index]) == (mapVertexToFileIndex(vertexPath[index + lookAhead]) - lookAhead)){
      std::cout << mapVertexToFileIndex(vertexPath[index]) << " and " << mapVertexToFileIndex(vertexPath[index + lookAhead]) - lookAhead << std::endl;
      lookAhead++;
    }
    if (lookAhead == 1){
      expressionString.append("IP + " + QString::number(mapVertexToFileIndex(vertexPath[index])) + ", ");
    }
    else{
      lookAhead--;
      expressionString.append("IP + " + QString::number(mapVertexToFileIndex(vertexPath[index])) + " : IP + " + QString::number(mapVertexToFileIndex(vertexPath[index+lookAhead])) + ", ");
      index += lookAhead;
    }
  }
  expressionString.chop(2);
  std::cout << expressionString.toStdString() << std::endl;
  return expressionString;
}

int GeoWriter::mapVertexToFileIndex(Vertex* vert){
  std::vector<int>::iterator startIter = std::find(vertexMap.begin(), vertexMap.end(), vert->vertexNumber);
  int ID = startIter - vertexMap.begin() + 1;
  return ID;
}

