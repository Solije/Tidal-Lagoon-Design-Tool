#ifndef VERTEX_H
#define VERTEX_H

#include <vector>
#include <memory>
#include <QGraphicsItem>
#include <QGraphicsSceneMoveEvent>
#include "mesh.h"
#include "meshelement.h"

class MeshEdge;
class Mesh;
class Spline;

class Vertex : public QGraphicsObject, public MeshElement
{
  Q_OBJECT
public:
  int vertexNumber = 0;
  QPointF previousPos;

  std::vector<Spline*> parentSplines;

  /*
   * Constructor
   */
  Vertex(int vertexNumber, double xPos, double yPos, double zPos, Mesh* parent);

  /*
   * Destructor
   */
  ~Vertex();

  /*
   * Defines the type of QGraphicsItem.
   * Allows the use of qgraphicsitem_cast() on the vertex
   */
  enum { Type = QGraphicsItem::UserType + 1 };
  int type() const;

  /*
   * Defines << operator for Vertex
   * simplifies syntax for printing Vertex information for debugging.
   */
  friend std::ostream& operator<<(std::ostream& os, const Vertex& vtx);

  ///////////////////////////////////////////
  // Line functions
  ///////////////////////////////////////////

  /*
   * Sets another MeshEdge as connected to this Vertex
   */
  void addLine(std::shared_ptr<MeshEdge> newLine);

  bool deleteLine(std::shared_ptr<MeshEdge> delLine);

  ///////////////////////////////////////////
  // "Get" functions
  ///////////////////////////////////////////

  /*
   * Returns a vector of the vertices linked to the given Vertex by a MeshEdge
   */
  std::vector<Vertex*> getNeighbours();


  /*
   * Returns a vector of the vertexNumbers of the vertices linked to the given vertex by a MeshEdge
   */
  std::vector<int> getNeighbourNumbers();

  /*
   * Returns a pointer to the MeshEdge joining the given Vertex to otherVertex
   */

  std::shared_ptr<MeshEdge> getLine(Vertex* otherVertex);

  /*
   * Returns a vector of the MeshEdges which are connected to the given Vertex
   */
  std::vector<std::shared_ptr<MeshEdge> > getLines();

  /*
   * Returns the position n Z axis
   */
  double getZPos();

  ///////////////////////////////////////////
  // Vertex Movement
  ///////////////////////////////////////////


  /*
   * Moves vertex to given position in the x-y plane. Z position is unaffected
   *
   * \param bool trackChange: Sets whether this movement should be entered into the changelog
   */
  void moveVertex(double xPosition, double yPosition, bool trackChange);

  /*
   * Moves vertex to given position in the x-y plane. Z position is unaffected
   *
   * \param bool trackChange: Sets whether this movement should be entered into the changelog
   */
  void moveVertex(QPointF newPosition, bool trackChange);


  /*
   * Moves vertex to given position in 3D space
   *
   * \param bool trackChange: Sets whether this movement should be entered into the changelog
   */
  void moveVertex(double xPosition, double yPosition, double zPosition, bool trackChange);


  /*
   * Sets new position in Z axis
   */
  void setZPos(double newZ);

  ///////////////////////////////////////////
  // Vertex Appearance
  ///////////////////////////////////////////

  /*
   * Gives size of bounding rectangle in which Vertex can be drawn
   */
  QRectF boundingRect() const override;

  /*
   * Gives shape of the object
   */
  QPainterPath shape() const;

  /*
   * Draws appearance of Vertex on scene
   */
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

  ///////////////////////////////////////////
  // Vertex status
  ///////////////////////////////////////////

  /*
   * Sets whether vertex is active
   * ie. the most recently moved vertex
   */
  void setActiveVertex(bool active);

  void setDeletionMarker(bool markerStatus);

  bool isMarkedForDeletion();

  ///////////////////////////////////////////
  // Vertex interactivity
  ///////////////////////////////////////////

  /*
   * Captures when a Vertex is clicked
   */
  void mousePressEvent(QGraphicsSceneMouseEvent *event);

  /*
   * Captures when a Vertex is dropped
   */
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

  /*
   * Opens a context menu to allow user to perform several actions on the vertex
   */
  void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);


public slots:

  /*
   * Redraws the MeshEdges linked to the given vertex such that they stay connected when the vertex is moved
   */
  void updateLines();


  bool addToPhysicalGroup();

signals:


  ///////////////////////////////////////////
  // Vertex movement
  ///////////////////////////////////////////

  void vertexSelected(int vertexNumber);

  /*
   * Signal denoting a vertex having moved
   * Passed to MainWindow so it knows to refresh the view
   */
  void vertexBeingMoved(int vertexNumber);

  /*
   * Signal denoting a vertex having stopped moving and dropped
   * emitted so that the movement can be added to the ledger
   */
  void vertexDropped(int vertexNumber);



protected:
  /*
   * Monitors if the Vertex is being moved
   */
  QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;


private:
  double zPos;

  /*
   * Colour of the Vertex
   * Qt::black when inactive
   * Qt::red when active
   */
  QColor colour = Qt::black;

  bool markedForDeletion = false;

  //Vector of all the MeshEdges which are conected to this Vertex
  std::vector<std::shared_ptr<MeshEdge> > lines;

};

#endif // VERTEX_H
