#include "vertex.h"

#include <iostream>

#include <QInputDialog>
#include <QtWidgets>

#include "UI/physicalgroupsdialog.h"
#include "meshview.h"
#include "UI/mainwindow.h"

Vertex::Vertex(int vertNumber, double xPosition, double yPosition, double zPosition, Mesh* mesh = 0): QGraphicsObject(), MeshElement(mesh){
  elementType = MeshElement::Point;
  vertexNumber = vertNumber;
  zPos = zPosition;
  setPos(xPosition,yPosition);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
  connect(this, SIGNAL(vertexBeingMoved(int)), this, SLOT(updateLines()));
  setFlag(QGraphicsObject::ItemIsSelectable, true);
//  setCacheMode(QGraphicsItem::ItemCoordinateCache);
}

Vertex::~Vertex(){

}

///////////////////////////////////////////
// "Get" functions
///////////////////////////////////////////

std::vector<Vertex*> Vertex::getNeighbours(){
  std::vector<Vertex*> neighbourList;
  for (std::shared_ptr<MeshEdge> line : getLines()){
    //Find other end of the line and get vertex attached to it
    if (line->startPoint != this){
      neighbourList.push_back(line->startPoint);
    } else {
      neighbourList.push_back(line->endPoint);
    }
  }
  return neighbourList;
}

std::vector<int> Vertex::getNeighbourNumbers(){
  std::vector<int> neighbourNumberList;
  for (Vertex* p : getNeighbours()){
    neighbourNumberList.push_back(p->vertexNumber);
  }
  return neighbourNumberList;
}

std::shared_ptr<MeshEdge> Vertex::getLine(Vertex* otherVertex){
  //Get number of Vertex on other end of line
  int otherVertexNumber = otherVertex->vertexNumber;
  for (std::shared_ptr<MeshEdge> p : getLines()){
      //Find start and end vertices of line
      int startVertexNumber = p->startPoint->vertexNumber;
      int endVertexNumber = p->endPoint->vertexNumber;
      //If a line with the same start and end vertices exists, return it.
      if (startVertexNumber == otherVertexNumber || endVertexNumber == otherVertexNumber){
        return p;
      }
    }
  //else return null
  return 0;
}

std::vector<std::shared_ptr<MeshEdge> > Vertex::getLines(){
  return lines;
}

double Vertex::getZPos(){
  return zPos;
}

///////////////////////////////////////////
// Vertex Addition and Deletion
///////////////////////////////////////////

void Vertex::addLine(std::shared_ptr<MeshEdge> newLine){
  lines.push_back(newLine);
}

bool Vertex::deleteLine(std::shared_ptr<MeshEdge> delLine){
  //Create temporary vector
  //This will replace the existing one
  std::vector<std::shared_ptr<MeshEdge> > tempLines;

  for (std::shared_ptr<MeshEdge> p : getLines()){
    //Add all existing lines which aren't the one to be deleted to tempLines
    if(p != delLine){
        tempLines.push_back(p);
      }
  };

  //Overwrite lines if line has been removed
  //Return whether line has been removed or not.
  if(tempLines.size() < lines.size()){
    lines = tempLines;
    return true;
  } else {
    return false;
  }

}

///////////////////////////////////////////
// Vertex Position functions
///////////////////////////////////////////

void Vertex::moveVertex(double xPosition, double yPosition, bool trackChange = false){
  previousPos = pos();
  setPos(xPosition,yPosition);
  if(trackChange){
    emit vertexDropped(vertexNumber);
  }
}

void Vertex::moveVertex(double xPosition, double yPosition, double zPosition, bool trackChange = false){
  moveVertex(xPosition, yPosition, trackChange);
  setZPos(zPosition);
}

void Vertex::moveVertex(QPointF newPosition, bool trackChange = false){
  moveVertex(newPosition.x(),newPosition.y(), trackChange);
}

void Vertex::setZPos(double newZ){
  zPos = newZ;
}

///////////////////////////////////////////
// Vertex status
///////////////////////////////////////////

bool Vertex::isMarkedForDeletion(){
  return markedForDeletion;
}

void Vertex::setDeletionMarker(bool markerStatus){
  markedForDeletion = markerStatus;
}

void Vertex::setActiveVertex(bool active){
  if(active){
    colour = Qt::red;
  } else {
    colour = Qt::black;
  }
}

///////////////////////////////////////////
// Vertex drawing functions
///////////////////////////////////////////

QRectF Vertex::boundingRect() const{
  QTransform t = this->transform() * qobject_cast<MeshView*>(this->parentMesh->parent())->transform();
  //Addition of this correction factor ensures that the boundingRect of the vertex is unchanged by scaling
  double correctionFactor = 1.0/sqrt(t.m11()*t.m11() + t.m12() * t.m12());
  double scaledSize = parentMesh->vertexRadius*correctionFactor;

  QRectF rectF = QRectF(-0.5*scaledSize, -0.5*scaledSize, scaledSize, scaledSize);
  return rectF;
}

QPainterPath Vertex::shape() const
{

    QTransform t = this->transform() * qobject_cast<MeshView*>(this->parentMesh->parent())->transform();
    //Addition of this correction factor ensures that the clickable area of the vertex is unchanged by scaling
    double correctionFactor = 1.0/sqrt(t.m11()*t.m11() + t.m12() * t.m12());
    double scaledSize = parentMesh->vertexRadius*correctionFactor;

    QPainterPath path;
    path.addEllipse(QPointF(0,0), 1.5 * scaledSize, 1.5 * scaledSize);
    return path;
}

void Vertex::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(option);
  Q_UNUSED(widget);

  QTransform t = painter->transform();
  //Addition of this correction factor prevents the vertex size from diverging when the view is rotated
  double correctionFactor = 1.0/sqrt(t.m11()*t.m11() + t.m12() * t.m12());
  painter->save(); // save painter state

  //Addition of this correction factor prevents the vertex size from diverging when the view is rotated
  painter->scale(correctionFactor, correctionFactor);

  if (this->isSelected()){
    QColor colour = Qt::red;
    //Circle outline colour
    painter->setPen(QPen(colour, 0));
    //Circle fill colour
    painter->setBrush(QBrush(colour));
  } else {
    //Circle outline colour
    painter->setPen(QPen(colour, 0));
    //Circle fill colour
    painter->setBrush(QBrush(colour));
  }
  painter->drawEllipse(QPointF(0,0), parentMesh->vertexRadius, parentMesh->vertexRadius);
  painter->restore();
}

void Vertex::updateLines(){
  for(std::shared_ptr<MeshEdge> p : lines){
    QPointF startPoint = p->startPoint->pos();
    QPointF endPoint = p->endPoint->pos();
    p->setLine(QLineF(startPoint, endPoint));
    p->setPen(QPen(p->pen().color(), 0));
//    std::cout << "Refreshing line from " << p->startPoint->vertexNumber << " to " << p->endPoint->vertexNumber << std::endl;
  }
}

///////////////////////////////////////////
// Vertex interactivity functions
///////////////////////////////////////////

QVariant Vertex::itemChange(GraphicsItemChange change, const QVariant &value){
  if (change == QGraphicsItem::ItemPositionHasChanged && scene()){
    // value is the new position.
    QPointF newPos = value.toPointF();
    emit vertexBeingMoved(vertexNumber);
    QRectF rect = scene()->sceneRect();
//    std::cout << "moved to " << newPos.x() << "," << newPos.y() << std::endl;
    if (!rect.contains(newPos)) {
      // Keep the item inside the scene rect.
      newPos.setX(qMin(rect.right(), qMax(newPos.x(), rect.left())));
      newPos.setY(qMin(rect.bottom(), qMax(newPos.y(), rect.top())));
      return newPos;
    }
  }
  return QGraphicsItem::itemChange(change, value);
}

void Vertex::mousePressEvent(QGraphicsSceneMouseEvent *event){
  if (event->button() == Qt::LeftButton){
    previousPos = pos();
    emit vertexSelected(vertexNumber);
  }
  QGraphicsObject::mousePressEvent(event);
}

void Vertex::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
//  std::cout << "Dropped vertex " << this->vertexNumber << std::endl;
  if (previousPos != pos()){
    emit vertexDropped(vertexNumber);
  }
  QGraphicsObject::mouseReleaseEvent(event);
}

void Vertex::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    QAction *physicalAction = menu.addAction("Add this vertex to a physical group", this, SLOT(addToPhysicalGroup()));
    QAction *deleteAction = menu.addAction("Delete Vertex");
    QAction *edgeAction = menu.addAction("Link this vertex to active vertex");
    QAction* deleteSpline;
    if (!parentSplines.empty()){
      deleteSpline = menu.addAction("Delete Splines");
    }
    QAction *selectedAction = menu.exec(event->screenPos());

    //Way suboptimal but works, fix later
    if (selectedAction == deleteAction){
      parentMesh->deleteVertex(this);
    }
    else if (selectedAction == edgeAction){
      parentMesh->addLine(this, qobject_cast<MeshView*>(parentMesh->parent())->getActiveVertex());
    }
    else if(selectedAction == deleteSpline){
      std::vector<Spline*> splineList = parentSplines;
      for (Spline* spline: splineList){
        parentMesh->deleteSpline(spline);
      }
      parentSplines.clear();
    }

}

///////////////////////////////////////////
// Physical groups
///////////////////////////////////////////

bool Vertex::addToPhysicalGroup(){
  PhysicalGroupsDialog* dialog = new PhysicalGroupsDialog(qobject_cast<QWidget*>(parentMesh->parent()), elementType, parentMesh);
  int response = dialog->exec();

  if (response == QDialog::Accepted){
    for (QCheckBox* checkBox : dialog->groupCheckboxes){
      if (checkBox->isChecked()){
        parentMesh->addToPhysGroup(this, checkBox->text());
      }
    }
    if (!dialog->getNewGroupName().isEmpty()){
      parentMesh->addToPhysGroup(this, dialog->getNewGroupName());
    }
    return true;
  }
  return false;
}

///////////////////////////////////////////
// Misc Functions
///////////////////////////////////////////

/*
 * Used by qgraphicsitem_cast to identify an item as a Vertex
 */
int Vertex::type() const{
  return Vertex::Type;
}

/*
 * Friend function of Vertex Class
 */
std::ostream& operator<<(std::ostream& os, const Vertex& vtx){
  os << "Vertex number: " << vtx.vertexNumber << " at (" << vtx.pos().x() << "," << vtx.pos().y() << ")";
  return os;
}
