#include "meshedge.h"

#include <iostream>

#include <QMenu>
#include <QInputDialog>

#include "Mesh/linesection.h"
#include "UI/physicalgroupsdialog.h"

MeshEdge::MeshEdge(Vertex* startVertex, Vertex* endVertex, int lineID) : QObject(startVertex->parentMesh), QGraphicsLineItem(), LineSection(startVertex->parentMesh)
{
  elementType = MeshElement::Line;
  startPoint = startVertex;
  endPoint = endVertex;
  ID = lineID;
  setLine(QLineF(startPosition(), endPosition()));
  setFlag(QGraphicsItem::ItemIsSelectable, false);
}

Vertex* MeshEdge::startVertex(){
  return startPoint;
}

Vertex* MeshEdge::endVertex(){
  return endPoint;
}

QPointF MeshEdge::startPosition(){
  return startPoint->pos();
}

QPointF MeshEdge::endPosition(){
  return endPoint->pos();
}

int MeshEdge::getID(){
  return ID;
}

void MeshEdge::updateLine(){
  QColor edgeColour;
  //Take colour of parent spline if possible, else be black
  if (parentSpline != nullptr){
    edgeColour = parentSpline->splineColour;
  } else {
    edgeColour = Qt::black;
  }
  //Draw scaled line with this colour
  setPen(QPen(edgeColour, 0));

}

void MeshEdge::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{


  QMenu menu;

  QAction* physicalAction;
  QAction* deleteSpline;
  if (parentSpline != nullptr){
    physicalAction = menu.addAction(QString("Add this spline to a physical group"), this, SLOT(addToPhysicalGroup()));
    deleteSpline = menu.addAction("Delete Spline");
  }
  QAction* deleteAction = menu.addAction("Delete Edge");
  QAction* bisectAction = menu.addAction("Bisect Edge", this, SLOT(bisect()));
  QAction* splitAction = menu.addAction("Split Edge in sections", this, SLOT(splitLine()));

  Q_UNUSED(bisectAction)
  Q_UNUSED(splitAction)

  QAction *selectedAction = menu.exec(event->screenPos());
  if (selectedAction == deleteAction){
    parentMesh->deleteLine(startPoint,endPoint);
  }
  else if(selectedAction == deleteSpline){
    parentMesh->deleteSpline(parentSpline);
  }
}

bool MeshEdge::addToPhysicalGroup(){
  PhysicalGroupsDialog* dialog = new PhysicalGroupsDialog(qobject_cast<QWidget*>(parentMesh->parent()), elementType, parentMesh);
  int response = dialog->exec();

  if (response == QDialog::Accepted){
    for (QCheckBox* checkBox : dialog->groupCheckboxes){
      if (checkBox->isChecked()){
        parentMesh->addToPhysGroup(parentSpline, checkBox->text());
      }
    }
    if (!dialog->getNewGroupName().isEmpty()){
      parentMesh->addToPhysGroup(parentSpline, dialog->getNewGroupName());
    }
    return true;
  }
  return false;
}

void MeshEdge::bisect(){
  split(2);
}

void MeshEdge::splitLine(){
  bool ok = 0;
  int sections = QInputDialog::getInt(qobject_cast<QWidget*>(this->parentMesh->parent()), QObject::tr("Split Line"),
                                       QObject::tr("Enter the number of sections you want to split this line into:"), 2, 1, 100, 1, &ok);
  if (ok && sections > 1){
    std::cout << "Splitting into " << sections << " sections" << std::endl;
    split(sections);
  }
}

void MeshEdge::split(int sections){

  //Calculate positions of new vertices
  std::vector<QPointF> vertexPositions;
  for (int i = 1; i < sections; i++){
    QPointF vertPos = startPosition() + (endPosition() - startPosition()) *(double(i)/sections);
    vertexPositions.push_back(vertPos);
  }

  //Construct these new vertices
  std::vector<Vertex*> newVertices;
  for (QPointF point : vertexPositions){
    Vertex* newVertex = new Vertex(parentMesh->vertexCount()+1, point.x(), point.y(), 0, parentMesh);
    parentMesh->addVertex(newVertex);
    newVertices.push_back(newVertex);
  }




  parentMesh->addLine(startPoint, newVertices.front());
  for (std::vector<Vertex*>::iterator iter = newVertices.begin(); iter < newVertices.end() - 1; iter++){
//    std::cout << (*iter)->vertexNumber << " "<< (*(iter+1))->vertexNumber << std::endl;
    parentMesh->addLine(*iter, *(iter+1));
  }
  parentMesh->addLine(newVertices.back(), endPoint);

  //Remove original line
  parentMesh->deleteLine(startPoint, endPoint);

  //If deleted MeshEdge was part of a spline
  //Find where new vertex would be in vertexPath and insert it
  if (parentSpline != nullptr){
      for (Vertex* vert : newVertices){
        vert->parentSplines.push_back(parentSpline);
      }
      //Don't know which way round the MeshEdge appears in Spline.
      //Search for both ends then choose the first occurance
      std::vector<Vertex*>::iterator startIter = std::find(parentSpline->verticesPath.begin(), parentSpline->verticesPath.end(), startPoint);
      std::vector<Vertex*>::iterator endIter = std::find(parentSpline->verticesPath.begin(), parentSpline->verticesPath.end(), endPoint);

      //Insert new vertex into path
      if (startIter - parentSpline->verticesPath.begin() < endIter - parentSpline->verticesPath.begin()){
        parentSpline->verticesPath.insert(startIter+1, newVertices.begin(), newVertices.end());
      } else {
        parentSpline->verticesPath.insert(endIter+1, newVertices.rbegin(), newVertices.rend());
      }

      //Find newly added MeshEdges
      parentSpline->generateLineList();

  }

}
