#include "spline.h"

#include "vertex.h"
#include <iostream>
#include <exception>


///////////////////////////////////////////
// Constructors
///////////////////////////////////////////

Spline::Spline(): LineSection()
{
  elementType = MeshElement::Line;
}

Spline::Spline(std::vector<Vertex*> pathWaypoints, QString physicalGroupName): LineSection(pathWaypoints.front()->parentMesh){

  elementType = MeshElement::Line;

  pathWaypoints.erase(std::unique(pathWaypoints.begin(), pathWaypoints.end()), pathWaypoints.end());
  std::cout << "Building spline between waypoints: " << vectorToString(pathWaypoints).toStdString() << std::endl;


  //Sets the valid routes from initial vertex to search for next waypoint on
  std::vector<std::vector<Vertex*> > validNeighbourVector;
  validNeighbourVector.push_back(pathWaypoints.front()->getNeighbours());

  unsigned int i = 0;
  while (i < pathWaypoints.size()-1){
   std::cout << "Finding route from " << pathWaypoints[i]->vertexNumber << " to " << pathWaypoints[i+1]->vertexNumber << " starting towards " << validNeighbourVector[i].back()->vertexNumber << std::endl;
   //Find a route to the next waypoint
   std::vector<Vertex*> subPath = testPath(pathWaypoints[i], validNeighbourVector[i].back(), pathWaypoints[i+1]);

   if (subPath.empty()){
     std::cout << "failed to find next waypoint" << std::endl;
     //If empty path is returned, must try a new path

     //Remove previously tried route out of waypoint
     validNeighbourVector.back().pop_back();

     while (validNeighbourVector[i].size() == 0){
       std::cout << "backtracking" << std::endl;
       if (i == 0){
         //If no valid route from first waypoint then must throw exception as spline is invalid
         std::cout << "Not a valid spline, throwing exception" << std::endl;
         throw std::exception();
       }
       //No valid route out of waypoint
       //Have to backtrace and try again

       //Remove the neighbour vector for last waypoint
       validNeighbourVector.pop_back();
       std::cout << "popped neighbours" << std::endl;
       subPaths.pop_back();
       std::cout << "popped subPath" << std::endl;

       //Return to previous waypoint
       i--;
       std::cout << i << std::endl;
       std::cout << vectorToString(validNeighbourVector[i]).toStdString() << std::endl;

       //Remove last neighbour from previous waypoint as it leads to a dead end
       validNeighbourVector.back().pop_back();

     }

   }
   else{
     //Found a route to next waypoint
     //Attempt to find next

     if (i < pathWaypoints.size()-2){
       //Find new set of routes out of waypoint
       std::vector<Vertex*> validNeighbours = subPath.back()->getNeighbours();
       //Remove route back along spline
       validNeighbours.erase(std::remove(validNeighbours.begin(), validNeighbours.end(), subPath[subPath.size()-2]), validNeighbours.end());

       std::cout << "After removing " << subPath[subPath.size()-2]->vertexNumber << ", valid neighbours of waypoint are:\n" << vectorToString(validNeighbours).toStdString() << std::endl << std::endl;

       validNeighbourVector.push_back(validNeighbours);
     }
     subPaths.push_back(subPath);

     //proceed to next waypoint
     i++;
   }
  }


  verticesPath = flattenPath(subPaths);
  std::cout << "final path is:" << std::endl << vectorToString(verticesPath).toStdString() << std::endl;

  generateLineList();
  closedLoop = (pathWaypoints.front() == pathWaypoints.back());

  for (Vertex* p : verticesPath){
    p->parentSplines.push_back(this);
  }


  //Randomly generates spline colour
  splineColour.setHsv(qrand() % 256, 0.5*255, 0.95*255);

  parentMesh->addToPhysGroup(this, physicalGroupName);
  std::cout << "finished constructing spline" << std::endl;
}

Spline::Spline(Vertex* givenVertex, Vertex* targetVertex, QString physicalGroupName): LineSection(givenVertex->parentMesh){

  elementType = MeshElement::Line;
  verticesPath = constructPath(givenVertex, targetVertex, givenVertex->getNeighbours());
  if (verticesPath.empty()){
    //If no path is found then throw exception as spline is not valid
    std::cout << "Not a valid spline, throwing exception" << std::endl;
    throw std::exception();
  }
  generateLineList();
  closedLoop = (givenVertex == targetVertex);

  for (Vertex* p : verticesPath){
    p->parentSplines.push_back(this);
  }
  for (std::shared_ptr<MeshEdge> p : listOfLines){
    p->parentSpline = this;
  }

  //Randomly generates spline colour
  splineColour.setHsv(qrand() % 256, 0.5*255, 0.95*255);

  parentMesh->addToPhysGroup(this, physicalGroupName);

  std::cout << "finished constructing spline" << std::endl;
}

Spline::~Spline(){
  for (Vertex* p : verticesPath){
    p->parentSplines.erase(std::remove(p->parentSplines.begin(), p->parentSplines.end(), this), p->parentSplines.end());
  }
  for (std::shared_ptr<MeshEdge> p : listOfLines){
    p->parentSpline = nullptr;
  }
  verticesPath.clear();
  listOfLines.clear();
}

///////////////////////////////////////////
// Path Construction
///////////////////////////////////////////

std::vector<Vertex*> Spline::constructPath(Vertex* givenVertex, Vertex* targetVertex, std::vector<Vertex*> givenVertexNeighbours){
  for (Vertex* testNeighbour : givenVertexNeighbours){
    std::cout << "Looking for target in branch towards " << testNeighbour->vertexNumber << " from " << givenVertex->vertexNumber << ":" << std::endl;
    std::vector<Vertex*> possiblePath = testPath(givenVertex, testNeighbour, targetVertex);

    if (!possiblePath.empty()){
      //If path is non-empty then it is a complete path
      std::cout << "Branch path:" << std::endl;
      std::string path;
      for (Vertex* p : possiblePath){
          path.append(std::to_string(p->vertexNumber) + " ");
      }
      std::cout << path << std::endl;
      return possiblePath;
    }
  }
  return std::vector<Vertex*>();
}

std::vector<Vertex*> Spline::testPath(Vertex* givenVertex, Vertex* startNeighbour, Vertex* targetVertex){
  //Assuption:
  //givenVertex is not at the end of a spline
  //(i.e. it has 2 neighbours)

  std::cout << "List of vertices which have been visited are:\n" << vectorToString(flattenPath(subPaths)).toStdString() << std::endl << std::endl;


  std::vector<Vertex*> tempPath;
  tempPath.push_back(givenVertex);

  Vertex* currentVertex = givenVertex;
  Vertex* neighbourVertex = startNeighbour;

  //While haven't closed loop or reached end of chain
  do {      
//    std::cout << "Adding vertex " << neighbourVertex->vertexNumber << " to path" << std::endl;
    tempPath.push_back(neighbourVertex);
    if (neighbourVertex == targetVertex){
      //Have found the target vertex
      return tempPath;
    }

    if (neighbourVertex->getNeighbours().size() > 2){
      // Fork in the spline
      // Search each fork using recursion
      std::cout << "\nReached fork" << std::endl;

      std::vector<Vertex*> forkBranch = exploreFork(currentVertex, neighbourVertex, targetVertex);

      if (forkBranch.back() == targetVertex){
        // targetVertex has been found on one of the branches
        std::cout << "Found target in branch" << std::endl;

        //Remove fork vertex from tempPath as will be contained in fork path
        tempPath.pop_back();
        tempPath.insert(tempPath.end(), forkBranch.begin(), forkBranch.end());
        return tempPath;
      }
      std::cout << "Couldn't find target in branch" << std::endl;
    }

    //Move to neighbour vertex and choose new neighbour
    //Possibility to backtrack so check that new neighbour isn't currentVertex first
    if (neighbourVertex->getNeighbours().front() != currentVertex){
      currentVertex = neighbourVertex;
      neighbourVertex = neighbourVertex->getNeighbours().front();
    } else {//if (neighbourVertex->getNeighbours().back() != currentVertex) {
      currentVertex = neighbourVertex;
      neighbourVertex = neighbourVertex->getNeighbours().back();
    }

  } while(notVisited(neighbourVertex, tempPath) && neighbourVertex != givenVertex && currentVertex->getNeighbours().size() >= 2);

  //Could not find path so return empty vector
  std::cout << "//////////////////////////////////////////////////" << std::endl;
  std::cout << "Target not on branch, backtrace and try new branch" << std::endl;
  std::cout << "//////////////////////////////////////////////////\n" << std::endl;
  return std::vector<Vertex*>();
}

std::vector<Vertex*> Spline::exploreFork(Vertex* currentVertex, Vertex* neighbourVertex, Vertex* targetVertex){
  std::vector<Vertex*> validNeighbours = neighbourVertex->getNeighbours();
  validNeighbours.erase(std::remove(validNeighbours.begin(), validNeighbours.end(), currentVertex), validNeighbours.end());
  std::cout << "Now searching along:" << vectorToString(validNeighbours).toStdString() << std::endl;
  return constructPath(neighbourVertex, targetVertex, validNeighbours);
}

void Spline::generateLineList(){
  //Remove reference to spline from old line segments
  for (std::shared_ptr<MeshEdge> p : listOfLines){
    p->parentSpline = nullptr;
  }
  //Clear out old lines
  listOfLines.clear();

  //Reconstruct list from vertices path
  std::vector<std::shared_ptr<MeshEdge> > tempLineList;

  for (auto p = verticesPath.begin(); p != verticesPath.end(); p++){
    if (p+1 != verticesPath.end()){
      std::shared_ptr<MeshEdge> line = (*p)->getLine(*(p+1));
//      std::cout << "(" << (*p)->vertexNumber << "," << (*(p+1))->vertexNumber <<")"<< std::endl;
      tempLineList.push_back(line);
    }
  }
  if (closedLoop){
    //If the spline is a closed loop, join first and last vertices
    tempLineList.push_back(verticesPath.back()->getLine(verticesPath.front()));
  }
  listOfLines = tempLineList;

  //Add reference to spline to new line segments
  for (std::shared_ptr<MeshEdge> p : listOfLines){
    p->parentSpline = this;
  }
}

///////////////////////////////////////////
// Helper functions
///////////////////////////////////////////

std::vector<Vertex*> Spline::flattenPath(std::vector<std::vector<Vertex*> > subPaths){
  std::vector<Vertex*> flattenedPath;
  for (unsigned int i = 0; i < subPaths.size();i++){
    std::vector<Vertex*> subPath = subPaths[i];
    if (i < subPaths.size() - 1){
      //subPaths overlap at waypoints.
      //Remove these while flattening vector.
      subPath.pop_back();
    }
    for (Vertex* vert : subPath){
      flattenedPath.push_back(vert);
    }
  }
  return flattenedPath;
}

bool Spline::notVisited(Vertex* testVertex, std::vector<Vertex*> extraVertices){
  std::vector<Vertex*> visitedList = flattenPath(subPaths);
  visitedList.insert(visitedList.end(), extraVertices.begin(), extraVertices.end());
  visitedList.erase(std::unique(visitedList.begin(), visitedList.end()), visitedList.end());
  std::cout << "List of vertices which have been visited are:\n" << vectorToString(visitedList).toStdString() << std::endl << std::endl;
  return (std::find(visitedList.begin(), visitedList.end(), testVertex) == visitedList.end());
}

///////////////////////////////////////////
// Misc functions
///////////////////////////////////////////

Vertex* Spline::startVertex(){
  return verticesPath.front();
}

Vertex* Spline::endVertex(){
  return verticesPath.back();
}

QString Spline::vectorToString(std::vector<Vertex*> pathVector){
  QString path;
  for (Vertex* vert : pathVector){
    path.append(QString::number(vert->vertexNumber) + " ");
  }
  return path;
}
