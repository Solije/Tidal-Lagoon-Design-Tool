#ifndef FIELD_H
#define FIELD_H

#include <QString>
#include "Mesh/mesh.h"

class GeoWriter;

class Field
{
public:
  Field();
  Field(QString fieldName, bool backgroundField);

  /*
   * Returns a QString formated to encode the field into a .geo file
   */
  virtual QString printFieldInfo(int fieldIndex, GeoWriter* writer);

  ///////////////////////////////////////////
  // Field Parameters
  ///////////////////////////////////////////

  QString fieldName;
  bool backgroundField = 0;
  Mesh* parentMesh = nullptr;


};

#endif // FIELD_H
