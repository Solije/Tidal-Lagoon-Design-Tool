#ifndef THRESHOLDFIELD_H
#define THRESHOLDFIELD_H

#include "Mesh/Fields/field.h"

class ThresholdField : public Field
{
public:
  ThresholdField();
  ThresholdField(QString fieldName, int newDistMax, int newDistMin, int newIField, int newLcMax, int newLcMin, bool newSigmoid, bool StopAtDistMax, bool backgroundField);

  /*
   * Returns a QString formated to encode the field into a .geo file
   */
  QString printFieldInfo(int fieldIndex, GeoWriter* writer);

  ///////////////////////////////////////////
  // Field Parameters
  ///////////////////////////////////////////

  int DistMax = 0;
  int DistMin = 0;
  int IField = 0;
  int LcMax = 0;
  int LcMin = 0;
  bool Sigmoid = false;
  bool StopAtDistMax = false;


};

#endif // THRESHOLDFIELD_H
