#ifndef ATTRACTORFIELD_H
#define ATTRACTORFIELD_H


#include "Mesh/Fields/field.h"

class Vertex;

class AttractorField : public Field
{
public:
  AttractorField();
  AttractorField(QString fieldName, QString newedgesList, QString newfacesList, int newfieldX, int newfieldY, int newfieldZ, int newnNodesByEdge, QString newnodeNumberString, bool backgroundField);

  /*
   * Returns a QString formated to encode the field into a .geo file
   */
  QString printFieldInfo(int fieldIndex, GeoWriter* writer);

  ///////////////////////////////////////////
  // Field Parameters
  ///////////////////////////////////////////


  QString nodeNumberString;
  std::vector<int> nodeNumberList;
  std::vector<Vertex*> nodeList;
  QString edgesList;
  QString facesList;
  int fieldX = 0;
  int fieldY = 0;
  int fieldZ = 0;
  int nNodesByEdge;

  ///////////////////////////////////////////
  // Helper Functions
  ///////////////////////////////////////////

  void buildNodeList();



};

#endif // ATTRACTORFIELD_H
