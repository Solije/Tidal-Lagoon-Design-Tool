#ifndef LINESECTION_H
#define LINESECTION_H

#include "Mesh/meshelement.h"

class Mesh;
class Vertex;

class LineSection: public MeshElement
{
public:
  LineSection(Mesh* mesh = 0);

  virtual Vertex* startVertex();
  virtual Vertex* endVertex();
};

#endif // LINESECTION_H
