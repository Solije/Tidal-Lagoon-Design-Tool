#ifndef MESHELEMENT_H
#define MESHELEMENT_H

#include <QString>

class Mesh;

class MeshElement
{
public:
  ///////////////////////////////////////////
  // Constructors
  ///////////////////////////////////////////

  MeshElement(Mesh* mesh = 0);
  virtual ~MeshElement();
  ///////////////////////////////////////////
  // Data Structures
  ///////////////////////////////////////////

  //Mesh which this meshElement belongs to
  Mesh* parentMesh = nullptr;

  //Name of PhysicalGroup this element belongs to
  QString physicalGroup;

  //Flag to distinguish points from lines (or from surfaces)
  //Used such that incompatible elements aren't added to the same physical group
  int elementType = undefined;
  enum { undefined = 0 };
  enum { Point = 1 };
  enum { Line = 2 };
};

#endif // MESHELEMENT_H
